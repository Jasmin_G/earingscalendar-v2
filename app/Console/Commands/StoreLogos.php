<?php

namespace App\Console\Commands;

use App\Ticker;

use Illuminate\Console\Command;

use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;

use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

use Symfony\Component\HttpFoundation\File\UploadedFile;

class StoreLogos extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'store:logos';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $downloaded = 0;
        $missing = 0;
        $tickers = Ticker::all();

        // Delete old images
        foreach ($tickers as $ticker) {
            File::delete('public/img/' . $ticker->logo . '.jpg');
        }


        foreach ($tickers as $ticker) {

            $uid = uniqid();
            $ticker->update(['logo' => $uid]);
            $url = 'https://logo.clearbit.com/:https://www.'.$ticker->domain.'/';
            $content = @file_get_contents($url);

            if($content){
                Image::make(file_get_contents($url))->save('public/img/' . $uid . '.jpg');
                $downloaded++;

            }else{

                $this->info( ' Logo '.$ticker->company_name. ' IS NOT DOWNLOADED');
                $missing++;
            }

        }
        $down_percent = $downloaded / 10;
        $miss_percent = $missing / 10;

        $this->info( ' We have downloaded '.$down_percent. '%' );
        $this->info( ' We have not downloaded '.$miss_percent. '%' );
    }

}
