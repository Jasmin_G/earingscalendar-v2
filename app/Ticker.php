<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticker extends Model
{
    //
    protected $guarded = [];
    protected $table = 'tickers';

    public function pictureUrl()
    {
        if ($this->logo && file_exists('img/' . $this->logo . '.jpg')) {
            return '/img/' . $this->logo . '.jpg';
        } else {
            return '/tmp/company.png';
        }
    }
}

