﻿/* Showing results for tic.xlsx */

/* CREATE TABLE */
CREATE TABLE tickers(
id DOUBLE,
ticker VARCHAR(100),
company_name VARCHAR(100),
domain VARCHAR(100),
logo VARCHAR(100),
updated_at VARCHAR(100)
);

/* INSERT QUERY NO: 1 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
1, 'WMT', 'Wal-Mart Stores', 'walmartstores.com', '', ''
);

/* INSERT QUERY NO: 2 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
2, 'XOM', 'Exxon Mobil', 'exxonmobil.com', '', ''
);

/* INSERT QUERY NO: 3 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
3, 'GE', 'General Motors', 'gm.com', '', ''
);

/* INSERT QUERY NO: 4 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
4, 'F', 'Ford Motor', 'ford.com', '', ''
);

/* INSERT QUERY NO: 5 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
5, '', 'Enron', 'enron.com', '', ''
);

/* INSERT QUERY NO: 6 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
6, 'GE', 'General Electric', 'ge.com', '', ''
);

/* INSERT QUERY NO: 7 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
7, 'C', 'Citigroup', 'citigroup.com', '', ''
);

/* INSERT QUERY NO: 8 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
8, 'CVX', 'ChevronTexaco', 'chevron.com', '', ''
);

/* INSERT QUERY NO: 9 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
9, 'IBM', 'Intl. Business Machines', 'ibm.com', '', ''
);

/* INSERT QUERY NO: 10 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
10, 'PM', 'Philip Morris', 'philipmorris.com', '', ''
);

/* INSERT QUERY NO: 11 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
11, 'VZ', 'Verizon Communications', 'verizon.com', '', ''
);

/* INSERT QUERY NO: 12 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
12, 'AIG', 'American International Group', 'aig.com', '', ''
);

/* INSERT QUERY NO: 13 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
13, 'AEP', 'American Electric Power', 'aep.com', '', ''
);

/* INSERT QUERY NO: 14 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
14, 'DUK', 'Duke Energy', 'duke-energy.com', '', ''
);

/* INSERT QUERY NO: 15 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
15, 'T', 'AT&T', 'att.com', '', ''
);

/* INSERT QUERY NO: 16 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
16, 'BA', 'Boeing', 'boeing.com', '', ''
);

/* INSERT QUERY NO: 17 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
17, 'EP', 'El Paso', 'elpaso.com', '', ''
);

/* INSERT QUERY NO: 18 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
18, 'HD', 'Home Depot', 'homedepot.com', '', ''
);

/* INSERT QUERY NO: 19 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
19, 'BAC', 'Bank of America Corp.', 'bankofamerica.com', '', ''
);

/* INSERT QUERY NO: 20 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
20, 'FNMA', 'Fannie Mae', 'fanniemae.com', '', ''
);

/* INSERT QUERY NO: 21 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
21, 'JPM', 'J.P. Morgan Chase & Co.', 'jpmorganchase.com', '', ''
);

/* INSERT QUERY NO: 22 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
22, 'KR', 'Kroger', 'kroger.com', '', ''
);

/* INSERT QUERY NO: 23 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
23, 'CAH', 'Cardinal Health', 'cardinal.com', '', ''
);

/* INSERT QUERY NO: 24 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
24, 'MRK', 'Merck', 'merck.com', '', ''
);

/* INSERT QUERY NO: 25 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
25, 'ADM', 'State Farm Insurance Cos.', 'statefarm.com', '', ''
);

/* INSERT QUERY NO: 26 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
26, 'RRI', 'Reliant Energy', 'reliantenergy.com', '', ''
);

/* INSERT QUERY NO: 27 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
27, 'T', 'SBC Communications', 'sbc.com', '', ''
);

/* INSERT QUERY NO: 28 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
28, 'HPQ', 'Hewlett-Packard', 'hp.com', '', ''
);

/* INSERT QUERY NO: 29 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
29, 'MS', 'Morgan Stanley Dean Witter', 'msdw.com', '', ''
);

/* INSERT QUERY NO: 30 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
30, 'DYN', 'Dynegy', 'dynegy.com', '', ''
);

/* INSERT QUERY NO: 31 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
31, 'MCK', 'McKesson', 'mckesson.com', '', ''
);

/* INSERT QUERY NO: 32 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
32, 'SHLD', 'Sears Roebuck', 'sears.com', '', ''
);

/* INSERT QUERY NO: 33 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
33, 'ILA', 'Aquila', 'utilicorp.com', '', ''
);

/* INSERT QUERY NO: 34 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
34, 'TGT', 'Target', 'target.com', '', ''
);

/* INSERT QUERY NO: 35 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
35, 'PG', 'Procter & Gamble', 'pg.com', '', ''
);

/* INSERT QUERY NO: 36 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
36, 'MER', 'Merrill Lynch', 'ml.com', '', ''
);

/* INSERT QUERY NO: 37 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
37, 'TWX', 'AOL Time Warner', 'aoltimewarner.com', '', ''
);

/* INSERT QUERY NO: 38 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
38, 'ABS', 'Albertson\'s', 'albertsons.com', '', ''
);

/* INSERT QUERY NO: 39 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
39, 'BRK/B', 'Berkshire Hathaway', 'berkshirehathaway.com', '', ''
);

/* INSERT QUERY NO: 40 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
40, 'SHLDQ', 'Kmart', 'bluelight.com', '', ''
);

/* INSERT QUERY NO: 41 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
41, 'FMCC', 'Freddie Mac', 'freddiemac.com', '', ''
);

/* INSERT QUERY NO: 42 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
42, 'WCOM', 'WorldCom', 'worldcom.com', '', ''
);

/* INSERT QUERY NO: 43 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
43, 'MRO', 'Marathon Oil', 'marathon.com', '', ''
);

/* INSERT QUERY NO: 44 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
44, 'COST', 'Costco Wholesale', 'costco.com', '', ''
);

/* INSERT QUERY NO: 45 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
45, 'SWY', 'Safeway', 'safeway.com', '', ''
);

/* INSERT QUERY NO: 46 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
46, 'HPQ', 'Compaq Computer', 'compaq.com', '', ''
);

/* INSERT QUERY NO: 47 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
47, 'JNJ', 'Johnson & Johnson', 'jnj.com', '', ''
);

/* INSERT QUERY NO: 48 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
48, 'COP', 'Conoco', 'conoco.com', '', ''
);

/* INSERT QUERY NO: 49 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
49, 'PFE', 'Pfizer', 'pfizer.com', '', ''
);

/* INSERT QUERY NO: 50 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
50, 'JCP', 'J.C. Penney', 'jcpenney.net', '', ''
);

/* INSERT QUERY NO: 51 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
51, 'MET', 'MetLife', 'metlife.com', '', ''
);

/* INSERT QUERY NO: 52 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
52, 'MIR', 'Mirant', 'mirant.com', '', ''
);

/* INSERT QUERY NO: 53 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
53, 'DELL', 'Dell Computer', 'dell.com', '', ''
);

/* INSERT QUERY NO: 54 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
54, 'GS', 'Goldman Sachs Group', 'gs.com', '', ''
);

/* INSERT QUERY NO: 55 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
55, 'UPS', 'United Parcel Service', 'ups.com', '', ''
);

/* INSERT QUERY NO: 56 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
56, 'MOT', 'Motorola', 'motorola.com', '', ''
);

/* INSERT QUERY NO: 57 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
57, 'ALL', 'Allstate', 'allstate.com', '', ''
);

/* INSERT QUERY NO: 58 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
58, 'TXU', 'TXU', 'txu.com', '', ''
);

/* INSERT QUERY NO: 59 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
59, 'UTX', 'United Technologies', 'utc.com', '', ''
);

/* INSERT QUERY NO: 60 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
60, 'DOW', 'Dow Chemical', 'dow.com', '', ''
);

/* INSERT QUERY NO: 61 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
61, 'CAG', 'ConAgra', 'conagra.com', '', ''
);

/* INSERT QUERY NO: 62 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
62, 'PRU', 'Prudential Financial', 'prudential.com', '', ''
);

/* INSERT QUERY NO: 63 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
63, 'PEP', 'PepsiCo', 'pepsico.com', '', ''
);

/* INSERT QUERY NO: 64 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
64, 'WSF', 'Wells Fargo', 'wellsfargo.com', '', ''
);

/* INSERT QUERY NO: 65 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
65, 'INTC', 'Intel', 'intel.com', '', ''
);

/* INSERT QUERY NO: 66 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
66, 'IP', 'International Paper', 'ipaper.com', '', ''
);

/* INSERT QUERY NO: 67 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
67, 'DLPH', 'Delphi', 'delphiauto.com', '', ''
);

/* INSERT QUERY NO: 68 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
68, 'S', 'Sprint', 'sprint.com', '', ''
);

/* INSERT QUERY NO: 69 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
69, 'VCIT', 'New York Life Insurance', 'newyorklife.com', '', ''
);

/* INSERT QUERY NO: 70 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
70, 'DD', 'E.I. du Pont de Nemours', 'dupont.com', '', ''
);

/* INSERT QUERY NO: 71 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
71, 'GPX', 'Georgia-Pacific', 'gp.com', '', ''
);

/* INSERT QUERY NO: 72 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
72, 'MSFT', 'Microsoft', 'microsoft.com', '', ''
);

/* INSERT QUERY NO: 73 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
73, 'DIS', 'Walt Disney', 'disney.com', '', ''
);

/* INSERT QUERY NO: 74 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
74, 'AET', 'Aetna', 'aetna.com', '', ''
);

/* INSERT QUERY NO: 75 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
75, 'IM', 'Ingram Micro', 'ingrammicro.com', '', ''
);

/* INSERT QUERY NO: 76 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
76, 'LU', 'Lucent Technologies', 'lucent.com', '', ''
);

/* INSERT QUERY NO: 77 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
77, 'LMT', 'LMT', 'lockheedmartin.com', '', ''
);

/* INSERT QUERY NO: 78 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
78, 'WAG', 'Walgreen', 'walgreens.com', '', ''
);

/* INSERT QUERY NO: 79 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
79, '', 'Bank One Corp.', 'bankone.com', '', ''
);

/* INSERT QUERY NO: 80 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
80, 'TRGIX', 'TIAA-CREF', 'tiaa-cref.org', '', ''
);

/* INSERT QUERY NO: 81 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
81, 'PSX', 'Phillips Petroleum', 'phillips66.com', '', ''
);

/* INSERT QUERY NO: 82 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
82, 'BLS', 'BellSouth', 'bellsouth.com', '', ''
);

/* INSERT QUERY NO: 83 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
83, 'HON', 'Honeywell International', 'honeywell.com', '', ''
);

/* INSERT QUERY NO: 84 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
84, 'UNH', 'UnitedHealth Group', 'unitedhealthgroup.com', '', ''
);

/* INSERT QUERY NO: 85 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
85, 'VIA/B', 'Viacom', 'viacom.com', '', ''
);

/* INSERT QUERY NO: 86 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
86, 'SVU', 'Supervalu', 'supervalu.com', '', ''
);

/* INSERT QUERY NO: 87 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
87, '', 'PG&E Corp.', 'pgecorp.com', '', ''
);

/* INSERT QUERY NO: 88 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
88, 'AA', 'Alcoa', 'alcoa.com', '', ''
);

/* INSERT QUERY NO: 89 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
89, 'AXP', 'American Express', 'americanexpress.com', '', ''
);

/* INSERT QUERY NO: 90 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
90, 'WB', 'Wachovia Corp.', 'wachovia.com', '', ''
);

/* INSERT QUERY NO: 91 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
91, 'BMY', 'Lehman Brothers Holdings', 'lehman.com', '', ''
);

/* INSERT QUERY NO: 92 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
92, 'CSCO', 'Cisco Systems', 'cisco.com', '', ''
);

/* INSERT QUERY NO: 93 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
93, 'CVS', 'CVS', 'cvs.com', '', ''
);

/* INSERT QUERY NO: 94 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
94, 'LOW', 'Lowe\'s', 'lowes.com', '', ''
);

/* INSERT QUERY NO: 95 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
95, 'SYY', 'Sysco', 'sysco.com', '', ''
);

/* INSERT QUERY NO: 96 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
96, 'BMY', 'Bristol-Myers Squibb', 'bms.com', '', ''
);

/* INSERT QUERY NO: 97 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
97, 'EDS', 'Electronic Data Systems', 'eds.com', '', ''
);

/* INSERT QUERY NO: 98 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
98, 'CAT', 'Caterpillar', 'cat.com', '', ''
);

/* INSERT QUERY NO: 99 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
99, 'KO', 'Coca-Cola', 'coca-cola.com', '', ''
);

/* INSERT QUERY NO: 100 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
100, 'ADM', 'Archer Daniels Midland', 'admworld.com', '', ''
);

/* INSERT QUERY NO: 101 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
101, 'AN', 'AutoNation', 'autonation.com', '', ''
);

/* INSERT QUERY NO: 102 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
102, 'Q', 'Qwest Communications', 'qwest.com', '', ''
);

/* INSERT QUERY NO: 103 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
103, 'FDX', 'FedEx', 'fedex.com', '', ''
);

/* INSERT QUERY NO: 104 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
104, 'MPGSX', 'Mass. Mutual Life Insurance', 'massmutual.com', '', ''
);

/* INSERT QUERY NO: 105 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
105, 'PHA', 'Pharmacia', 'pharmacia.com', '', ''
);

/* INSERT QUERY NO: 106 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
106, 'FBF', 'FleetBoston Financial', 'fleet.com', '', ''
);

/* INSERT QUERY NO: 107 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
107, 'CI', 'Cigna', 'cigna.com', '', ''
);

/* INSERT QUERY NO: 108 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
108, 'AMR', 'AMR', 'amrcorp.com', '', ''
);

/* INSERT QUERY NO: 109 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
109, 'L', 'Loews', 'loews.com', '', ''
);

/* INSERT QUERY NO: 110 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
110, 'SLR', 'Solectron', 'solectron.com', '', ''
);

/* INSERT QUERY NO: 111 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
111, 'JCI', 'Johnson Controls', 'johnsoncontrols.com', '', ''
);

/* INSERT QUERY NO: 112 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
112, 'SUNW', 'Sun Microsystems', 'sun.com', '', ''
);

/* INSERT QUERY NO: 113 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
113, 'HCA', 'HCA', 'hcahealthcare.com', '', ''
);

/* INSERT QUERY NO: 114 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
114, 'VC', 'Visteon', 'visteon.com', '', ''
);

/* INSERT QUERY NO: 115 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
115, 'SLE', 'Sara Lee', 'saralee.com', '', ''
);

/* INSERT QUERY NO: 116 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
116, 'WM', 'Washington Mutual', 'wamu.com', '', ''
);

/* INSERT QUERY NO: 117 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
117, 'TECD', 'Tech Data', 'techdata.com', '', ''
);

/* INSERT QUERY NO: 118 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
118, 'FD', 'Federated Department Stores', 'federated-fds.com', '', ''
);

/* INSERT QUERY NO: 119 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
119, 'RTN', 'Raytheon', 'raytheon.com', '', ''
);

/* INSERT QUERY NO: 120 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
120, 'XRX', 'Xerox', 'xerox.com', '', ''
);

/* INSERT QUERY NO: 121 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
121, 'USB', 'U.S. Bancorp', 'usbank.com', '', ''
);

/* INSERT QUERY NO: 122 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
122, 'TRW', 'TRW', 'trw.com', '', ''
);

/* INSERT QUERY NO: 123 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
123, 'ABT', 'Abbott Laboratories', 'abbott.com', '', ''
);

/* INSERT QUERY NO: 124 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
124, 'NWE', 'Northwestern Mutual', 'northwesternmutual.com', '', ''
);

/* INSERT QUERY NO: 125 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
125, 'UAL', 'UAL', 'united.com', '', ''
);

/* INSERT QUERY NO: 126 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
126, 'MMM', 'Minnesota Mining & Mfg.', '3m.com', '', ''
);

/* INSERT QUERY NO: 127 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
127, 'ABC', 'AmerisourceBergen', 'amerisourcebergen.net', '', ''
);

/* INSERT QUERY NO: 128 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
128, 'CCE', 'Coca-Cola Enterprises', 'cokecce.com', '', ''
);

/* INSERT QUERY NO: 129 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
129, 'FLMNG', 'Fleming', 'fleming.com', '', ''
);

/* INSERT QUERY NO: 130 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
130, 'EMR', 'Emerson Electric', 'gotoemerson.com', '', ''
);

/* INSERT QUERY NO: 131 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
131, 'BBY', 'Best Buy', 'bestbuy.com', '', ''
);

/* INSERT QUERY NO: 132 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
132, 'RAD', 'Rite Aid', 'riteaid.com', '', ''
);

/* INSERT QUERY NO: 133 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
133, 'PUSH', 'Publix Super Markets', 'publix.com', '', ''
);

/* INSERT QUERY NO: 134 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
134, 'HIG', 'Hartford Financial Services', 'thehartford.com', '', ''
);

/* INSERT QUERY NO: 135 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
135, 'EXC', 'Exelon', 'exeloncorp.com', '', ''
);

/* INSERT QUERY NO: 136 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
136, 'NFS', 'Nationwide', 'nationwide.com', '', ''
);

/* INSERT QUERY NO: 137 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
137, 'XEL', 'Xcel Energy', 'xcelenergy.com', '', ''
);

/* INSERT QUERY NO: 138 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
138, 'VLO', 'Valero Energy', 'valero.com', '', ''
);

/* INSERT QUERY NO: 139 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
139, 'MCD', 'McDonald\'s', 'mcdonalds.com', '', ''
);

/* INSERT QUERY NO: 140 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
140, 'WY', 'Weyerhaeuser', 'weyerhaeuser.com', '', ''
);

/* INSERT QUERY NO: 141 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
141, 'KMB', 'Kimberly-Clark', 'kimberly-clark.com', '', ''
);

/* INSERT QUERY NO: 142 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
142, 'LMAC', 'Liberty Mutual Insurance Group', 'libertymutual.com', '', ''
);

/* INSERT QUERY NO: 143 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
143, 'MAY', 'May Department Stores', 'maycompany.com', '', ''
);

/* INSERT QUERY NO: 144 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
144, 'GT', 'Goodyear Tire & Rubber', 'goodyear.com', '', ''
);

/* INSERT QUERY NO: 145 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
145, 'WYE', 'Wyeth', 'wyeth.com', '', ''
);

/* INSERT QUERY NO: 146 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
146, 'OXY', 'Occidental Petroleum', 'oxy.com', '', ''
);

/* INSERT QUERY NO: 147 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
147, 'HS', 'Household International', 'household.com', '', ''
);

/* INSERT QUERY NO: 148 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
148, 'DAL', 'Delta Air Lines', 'delta.com', '', ''
);

/* INSERT QUERY NO: 149 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
149, 'GPS', 'Gap', 'gap.com', '', ''
);

/* INSERT QUERY NO: 150 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
150, 'LEA', 'Lear', 'lear.com', '', ''
);

/* INSERT QUERY NO: 151 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
151, 'NOC', 'Northrop Grumman', 'northropgrumman.com', '', ''
);

/* INSERT QUERY NO: 152 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
152, 'AHC', 'Amerada Hess', 'hess.com', '', ''
);

/* INSERT QUERY NO: 153 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
153, 'HAL', 'Halliburton', 'halliburton.com', '', ''
);

/* INSERT QUERY NO: 154 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
154, 'DE', 'Deere', 'deere.com', '', ''
);

/* INSERT QUERY NO: 155 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
155, 'EK', 'Eastman Kodak', 'kodak.com', '', ''
);

/* INSERT QUERY NO: 156 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
156, 'CMS', 'CMS Energy', 'cmsenergy.com', '', ''
);

/* INSERT QUERY NO: 157 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
157, 'CC', 'Circuit City Stores', 'circuitcity.com', '', ''
);

/* INSERT QUERY NO: 158 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
158, 'CIN', 'Cinergy', 'cinergy.com', '', ''
);

/* INSERT QUERY NO: 159 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
159, 'BUD', 'Anheuser-Busch', 'anheuser-busch.com', '', ''
);

/* INSERT QUERY NO: 160 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
160, 'WINN', 'Winn-Dixie Stores', 'winn-dixie.com', '', ''
);

/* INSERT QUERY NO: 161 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
161, 'AVT', 'Avnet', 'avnet.com', '', ''
);

/* INSERT QUERY NO: 162 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
162, 'WLP', 'WellPoint Health Networks', 'wellpoint.com', '', ''
);

/* INSERT QUERY NO: 163 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
163, 'SUN', 'Sunoco', 'sunocoinc.com', '', ''
);

/* INSERT QUERY NO: 164 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
164, 'TXT', 'Textron', 'textron.com', '', ''
);

/* INSERT QUERY NO: 165 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
165, 'EIX', 'Edison International', 'edison.com', '', ''
);

/* INSERT QUERY NO: 166 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
166, 'GD', 'General Dynamics', 'gd.com', '', ''
);

/* INSERT QUERY NO: 167 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
167, 'THC', 'Tenet Healthcare', 'tenethealth.com', '', ''
);

/* INSERT QUERY NO: 168 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
168, 'UNP', 'Union Pacific', 'up.com', '', ''
);

/* INSERT QUERY NO: 169 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
169, 'PHSY', 'PacifiCare Health Systems', 'pacificare.com', '', ''
);

/* INSERT QUERY NO: 170 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
170, 'FPI', 'Farmland Industries', 'farmland.com', '', ''
);

/* INSERT QUERY NO: 171 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
171, 'LLY', 'Eli Lilly', 'lilly.com', '', ''
);

/* INSERT QUERY NO: 172 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
172, 'WMI', 'Waste Management', 'wm.com', '', ''
);

/* INSERT QUERY NO: 173 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
173, 'ODP', 'Office Depot', 'officedepot.com', '', ''
);

/* INSERT QUERY NO: 174 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
174, 'WSM', 'Williams', 'williams.com', '', ''
);

/* INSERT QUERY NO: 175 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
175, '', 'Toys `R` Us', 'toysrus.com', '', ''
);

/* INSERT QUERY NO: 176 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
176, 'ORCL', 'Oracle', 'oracle.com', '', ''
);

/* INSERT QUERY NO: 177 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
177, 'TSN', 'Tyson Foods', 'tyson.com', '', ''
);

/* INSERT QUERY NO: 178 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
178, 'SPLS', 'Staples', 'staples.com', '', ''
);

/* INSERT QUERY NO: 179 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
179, 'TJX', 'TJX', 'tjx.com', '', ''
);

/* INSERT QUERY NO: 180 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
180, 'D', 'Dominion Resources', 'dom.com', '', ''
);

/* INSERT QUERY NO: 181 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
181, 'CSC', 'Computer Sciences', 'csc.com', '', ''
);

/* INSERT QUERY NO: 182 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
182, 'MAN', 'Manpower', 'manpower.com', '', ''
);

/* INSERT QUERY NO: 183 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
183, 'DHR', 'Dana', 'dana.com', '', ''
);

/* INSERT QUERY NO: 184 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
184, 'ANTM', 'Anthem', 'anthem.com', '', ''
);

/* INSERT QUERY NO: 185 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
185, 'AYE', 'Allegheny Energy', 'alleghenyenergy.com', '', ''
);

/* INSERT QUERY NO: 186 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
186, 'WHR', 'Whirlpool', 'whirlpoolcorp.com', '', ''
);

/* INSERT QUERY NO: 187 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
187, 'HUM', 'Humana', 'humana.com', '', ''
);

/* INSERT QUERY NO: 188 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
188, 'SUG', 'Southern', 'southernco.com', '', ''
);

/* INSERT QUERY NO: 189 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
189, 'MAR', 'Marriott International', 'marriott.com', '', ''
);

/* INSERT QUERY NO: 190 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
190, 'KRB', 'MBNA', 'mbna.com', '', ''
);

/* INSERT QUERY NO: 191 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
191, 'ARW', 'Arrow Electronics', 'arrow.com', '', ''
);

/* INSERT QUERY NO: 192 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
192, 'HNT', 'Health Net', 'health.net', '', ''
);

/* INSERT QUERY NO: 193 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
193, 'MMC', 'Marsh & McLennan', 'mmc.com', '', ''
);

/* INSERT QUERY NO: 194 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
194, 'NWA', 'Northwest Airlines', 'nwa.com', '', ''
);

/* INSERT QUERY NO: 195 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
195, 'PEG', 'Public Service Enterprise Group', 'pseg.com', '', ''
);

/* INSERT QUERY NO: 196 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
196, 'SGP', 'Schering-Plough', 'schering-plough.com', '', ''
);

/* INSERT QUERY NO: 197 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
197, 'ITW', 'Illinois Tool Works', 'itw.com', '', ''
);

/* INSERT QUERY NO: 198 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
198, 'CMCSK', 'Comcast', 'comcast.com', '', ''
);

/* INSERT QUERY NO: 199 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
199, 'ED', 'Consolidated Edison', 'conedison.com', '', ''
);

/* INSERT QUERY NO: 200 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
200, 'ETR', 'Entergy', 'entergy.com', '', ''
);

/* INSERT QUERY NO: 201 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
201, 'AES', 'AES', 'aesc.com', '', ''
);

/* INSERT QUERY NO: 202 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
202, 'AFL', 'AFLAC', 'aflac.com', '', ''
);

/* INSERT QUERY NO: 203 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
203, '', 'NiSource', 'nisource.com', '', ''
);

/* INSERT QUERY NO: 204 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
204, 'NKE', 'Nike', 'nike.com', '', ''
);

/* INSERT QUERY NO: 205 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
205, 'UNM', 'UnumProvident', 'unum.com', '', ''
);

/* INSERT QUERY NO: 206 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
206, 'HNZ', 'H.J. Heinz', 'heinz.com', '', ''
);

/* INSERT QUERY NO: 207 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
207, 'CL', 'Colgate-Palmolive', 'colgate.com', '', ''
);

/* INSERT QUERY NO: 208 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
208, 'LTD', 'Limited', 'limited.com', '', ''
);

/* INSERT QUERY NO: 209 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
209, 'BTO', 'John Hancock Financial Services', 'jhancock.com', '', ''
);

/* INSERT QUERY NO: 210 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
210, 'ESRX', 'Express Scripts', 'express-scripts.com', '', ''
);

/* INSERT QUERY NO: 211 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
211, 'BNI', 'Burlington Northern Santa Fe', 'bnsf.com', '', ''
);

/* INSERT QUERY NO: 212 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
212, 'A', 'Agilent Technologies', 'agilent.com', '', ''
);

/* INSERT QUERY NO: 213 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
213, 'NCC', 'National City Corp.', 'nationalcity.com', '', ''
);

/* INSERT QUERY NO: 214 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
214, 'FLR', 'Fluor', 'fluor.com', '', ''
);

/* INSERT QUERY NO: 215 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
215, 'USAA', 'United Services Automobile Assn.', 'usaa.com', '', ''
);

/* INSERT QUERY NO: 216 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
216, 'CAL', 'Continental Airlines', 'continental.com', '', ''
);

/* INSERT QUERY NO: 217 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
217, 'CD', 'Cendant', 'cendant.com', '', ''
);

/* INSERT QUERY NO: 218 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
218, 'TRV', 'St. Paul Cos.', 'stpaul.com', '', ''
);

/* INSERT QUERY NO: 219 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
219, 'MSFT', 'Guardian Life Ins. Co. of America', 'glic.com', '', ''
);

/* INSERT QUERY NO: 220 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
220, 'K', 'Kellogg', 'kelloggs.com', '', ''
);

/* INSERT QUERY NO: 221 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
221, 'PFG', 'Principal Financial', 'principal.com', '', ''
);

/* INSERT QUERY NO: 222 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
222, 'SCI', 'SCI Systems', 'sci.com', '', ''
);

/* INSERT QUERY NO: 223 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
223, 'BSC', 'Bear Stearns', 'bearstearns.com', '', ''
);

/* INSERT QUERY NO: 224 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
224, 'RAI', 'R.J. Reynolds Tobacco', 'rjrt.com', '', ''
);

/* INSERT QUERY NO: 225 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
225, 'ASH', 'Ashland', 'ashland.com', '', ''
);

/* INSERT QUERY NO: 226 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
226, 'FPL', 'FPL Group', 'fplgroup.com', '', ''
);

/* INSERT QUERY NO: 227 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
227, 'PGN', 'Progress Energy', 'progress-energy.com', '', ''
);

/* INSERT QUERY NO: 228 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
228, 'PBG', 'Pepsi Bottling', 'pbg.com', '', ''
);

/* INSERT QUERY NO: 229 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
229, 'STI', 'SunTrust Banks', 'suntrust.com', '', ''
);

/* INSERT QUERY NO: 230 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
230, 'DDS', 'Dillard\'s', 'dillards.com', '', ''
);

/* INSERT QUERY NO: 231 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
231, 'SSCC', 'Smurfit-Stone Container', 'smurfit-stone.net', '', ''
);

/* INSERT QUERY NO: 232 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
232, 'APC', 'Anadarko Petroleum', 'anadarko.com', '', ''
);

/* INSERT QUERY NO: 233 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
233, 'MAS', 'Masco', 'masco.com', '', ''
);

/* INSERT QUERY NO: 234 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
234, 'LCC', 'US Airways Group', 'usairways.com', '', ''
);

/* INSERT QUERY NO: 235 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
235, 'GPC', 'Genuine Parts', 'genpt.com', '', ''
);

/* INSERT QUERY NO: 236 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
236, 'TXN', 'Texas Instruments', 'ti.com', '', ''
);

/* INSERT QUERY NO: 237 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
237, 'PPG', 'PPG Industries', 'ppg.com', '', ''
);

/* INSERT QUERY NO: 238 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
238, 'CSX', 'CSX', 'csx.com', '', ''
);

/* INSERT QUERY NO: 239 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
239, 'CNO', 'Conseco', 'conseco.com', '', ''
);

/* INSERT QUERY NO: 240 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
240, 'GILLETTE', 'Gillette', 'gillette.com', '', ''
);

/* INSERT QUERY NO: 241 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
241, 'SRE', 'Sempra Energy', 'sempra.com', '', ''
);

/* INSERT QUERY NO: 242 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
242, 'FE', 'FirstEnergy', 'firstenergycorp.com', '', ''
);

/* INSERT QUERY NO: 243 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
243, 'CCU', 'Clear Channel Communications', 'clearchannel.com', '', ''
);

/* INSERT QUERY NO: 244 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
244, 'CHSCP', 'Cenex Harvest States', 'chsco-ops.com', '', ''
);

/* INSERT QUERY NO: 245 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
245, 'DTE', 'DTE Energy', 'dteenergy.com', '', ''
);

/* INSERT QUERY NO: 246 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
246, 'RMK', 'Aramark', 'aramark.com', '', ''
);

/* INSERT QUERY NO: 247 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
247, 'AOC', 'Aon', 'aon.com', '', ''
);

/* INSERT QUERY NO: 248 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
248, 'BAX', 'Baxter International', 'baxter.com', '', ''
);

/* INSERT QUERY NO: 249 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
249, 'CB', 'Chubb', 'chubb.com', '', ''
);

/* INSERT QUERY NO: 250 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
250, 'AT', 'Alltel', 'alltel.com', '', ''
);

/* INSERT QUERY NO: 251 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
251, 'CPN', 'Calpine', 'calpine.com', '', ''
);

/* INSERT QUERY NO: 252 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
252, 'NXTL', 'Nextel Communications', 'nextel.com', '', ''
);

/* INSERT QUERY NO: 253 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
253, 'KSS', 'Kohl\'s', 'kohls.com', '', ''
);

/* INSERT QUERY NO: 254 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
254, 'PGR', 'Progressive', 'progressive.com', '', ''
);

/* INSERT QUERY NO: 255 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
255, 'ASD', 'American Standard', 'americanstandard.com', '', ''
);

/* INSERT QUERY NO: 256 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
256, 'OMX', 'Boise Cascade', 'bc.com', '', ''
);

/* INSERT QUERY NO: 257 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
257, 'KEY', 'KeyCorp', 'keybank.com', '', ''
);

/* INSERT QUERY NO: 258 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
258, 'AMAT', 'Applied Materials', 'appliedmaterials.com', '', ''
);

/* INSERT QUERY NO: 259 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
259, 'ETN', 'Eaton', 'eaton.com', '', ''
);

/* INSERT QUERY NO: 260 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
260, 'COF', 'Capital One Financial', 'capitalone.com', '', ''
);

/* INSERT QUERY NO: 261 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
261, 'BK', 'Bank of New York Co.', 'bankofny.com', '', ''
);

/* INSERT QUERY NO: 262 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
262, 'CCK', 'Crown Cork & Seal', 'crowncork.com', '', ''
);

/* INSERT QUERY NO: 263 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
263, 'EMC', 'EMC', 'emc.com', '', ''
);

/* INSERT QUERY NO: 264 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
264, 'GIS', 'General Mills', 'generalmills.com', '', ''
);

/* INSERT QUERY NO: 265 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
265, 'CMX', 'AdvancePCS', 'advparadigm.com', '', ''
);

/* INSERT QUERY NO: 266 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
266, 'ADP', 'Automatic Data Processing', 'adp.com', '', ''
);

/* INSERT QUERY NO: 267 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
267, 'SAFC', 'Safeco', 'safeco.com', '', ''
);

/* INSERT QUERY NO: 268 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
268, 'YUM', 'Tricon Global Restaurants', 'triconglobal.com', '', ''
);

/* INSERT QUERY NO: 269 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
269, 'PNX', 'PNC Financial Services Group', 'pnc.com', '', ''
);

/* INSERT QUERY NO: 270 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
270, 'NWL', 'Newell Rubbermaid', 'newellco.com', '', ''
);

/* INSERT QUERY NO: 271 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
271, 'KSE', 'KeySpan', 'keyspanenergy.com', '', ''
);

/* INSERT QUERY NO: 272 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
272, 'OMC', 'Omnicom Group', 'omnicomgroup.com', '', ''
);

/* INSERT QUERY NO: 273 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
273, 'NU', 'Northeast Utilities', 'nu.com', '', ''
);

/* INSERT QUERY NO: 274 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
274, 'PAA', 'Plains All American Pipeline', 'paalp.com', '', ''
);

/* INSERT QUERY NO: 275 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
275, 'ARM', 'ArvinMeritor', 'arvinmeritor.com', '', ''
);

/* INSERT QUERY NO: 276 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
276, 'OKE', 'Oneok', 'oneok.com', '', ''
);

/* INSERT QUERY NO: 277 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
277, 'AV', 'Avaya', 'avaya.com', '', ''
);

/* INSERT QUERY NO: 278 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
278, 'CVX', 'Unocal', 'unocal.com', '', ''
);

/* INSERT QUERY NO: 279 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
279, 'IPG', 'Interpublic Group', 'interpublic.com', '', ''
);

/* INSERT QUERY NO: 280 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
280, 'NAV', 'Navistar International', 'navistar.com', '', ''
);

/* INSERT QUERY NO: 281 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
281, 'CTX', 'Centex', 'centex.com', '', ''
);

/* INSERT QUERY NO: 282 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
282, 'CPB', 'Campbell Soup', 'campbellsoup.com', '', ''
);

/* INSERT QUERY NO: 283 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
283, 'FITB', 'FITB', '53.com', '', ''
);

/* INSERT QUERY NO: 284 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
284, 'FDC', 'First Data', 'firstdata.com', '', ''
);

/* INSERT QUERY NO: 285 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
285, 'PCO', 'Premcor', 'premcor.com', '', ''
);

/* INSERT QUERY NO: 286 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
286, 'LNC', 'Lincoln National', 'lfg.com', '', ''
);

/* INSERT QUERY NO: 287 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
287, 'GCI', 'Gannett', 'gannett.com', '', ''
);

/* INSERT QUERY NO: 288 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
288, 'SAH', 'Sonic Automotive', 'sonicautomotive.com', '', ''
);

/* INSERT QUERY NO: 289 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
289, 'GLW', 'Corning', 'corning.com', '', ''
);

/* INSERT QUERY NO: 290 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
290, 'DF', 'Dean Foods', 'deanfoods.com', '', ''
);

/* INSERT QUERY NO: 291 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
291, 'TFC', 'BB&T Corp.', 'bbandt.com', '', ''
);

/* INSERT QUERY NO: 292 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
292, 'UAG', 'United Auto Group', 'unitedauto.com', '', ''
);

/* INSERT QUERY NO: 293 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
293, 'NSC', 'Norfolk Southern', 'nscorp.com', '', ''
);

/* INSERT QUERY NO: 294 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
294, 'SAIC', 'Science Applications Intl.', 'saic.com', '', ''
);

/* INSERT QUERY NO: 295 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
295, 'PCAR', 'Paccar', 'paccar.com', '', ''
);

/* INSERT QUERY NO: 296 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
296, 'GTW', 'Gateway', 'gateway.com', '', ''
);

/* INSERT QUERY NO: 297 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
297, 'SKS', 'Saks', 'saksincorporated.com', '', ''
);

/* INSERT QUERY NO: 298 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
298, 'LEN', 'Lennar', 'lennar.com', '', ''
);

/* INSERT QUERY NO: 299 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
299, 'NAV', 'Avista', 'avistacorp.com', '', ''
);

/* INSERT QUERY NO: 300 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
300, 'UIS', 'Unisys', 'unisys.com', '', ''
);

/* INSERT QUERY NO: 301 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
301, 'OI', 'Owens-Illinois', 'o-i.com', '', ''
);

/* INSERT QUERY NO: 302 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
302, 'AVP', 'Avon Products', 'avon.com', '', ''
);

/* INSERT QUERY NO: 303 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
303, 'PH', 'Parker Hannifin', 'parker.com', '', ''
);

/* INSERT QUERY NO: 304 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
304, 'NCR', 'NCR', 'ncr.com', '', ''
);

/* INSERT QUERY NO: 305 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
305, 'SFD', 'Smithfield Foods', 'smithfieldfoods.com', '', ''
);

/* INSERT QUERY NO: 306 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
306, 'ROH', 'Rohm & Haas', 'rohmhaas.com', '', ''
);

/* INSERT QUERY NO: 307 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
307, 'PHI', 'Conectiv', 'conectiv.com', '', ''
);

/* INSERT QUERY NO: 308 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
308, 'SVM', 'ServiceMaster', 'svm.com', '', ''
);

/* INSERT QUERY NO: 309 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
309, 'PPL', 'PPL', 'pplweb.com', '', ''
);

/* INSERT QUERY NO: 310 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
310, 'APD', 'Air Products & Chemicals', 'airproducts.com', '', ''
);

/* INSERT QUERY NO: 311 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
311, 'CMI', 'Cummins', 'cummins.com', '', ''
);

/* INSERT QUERY NO: 312 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
312, 'IDA', 'Idacorp', 'idacorpinc.com', '', ''
);

/* INSERT QUERY NO: 313 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
313, 'STT', 'State Street Corp.', 'statestreet.com', '', ''
);

/* INSERT QUERY NO: 314 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
314, 'JWN', 'Nordstrom', 'nordstrom.com', '', ''
);

/* INSERT QUERY NO: 315 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
315, 'CMX', 'Caremark Rx', 'caremarkrx.com', '', ''
);

/* INSERT QUERY NO: 316 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
316, 'AW', 'Allied Waste Industries', 'alliedwaste.com', '', ''
);

/* INSERT QUERY NO: 317 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
317, 'LUV', 'Southwest Airlines', 'southwest.com', '', ''
);

/* INSERT QUERY NO: 318 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
318, 'MDT', 'Medtronic', 'medtronic.com', '', ''
);

/* INSERT QUERY NO: 319 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
319, 'PVN', 'Providian Financial', 'providian.com', '', ''
);

/* INSERT QUERY NO: 320 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
320, 'VF', 'VF', 'vfc.com', '', ''
);

/* INSERT QUERY NO: 321 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
321, 'FDML', 'Federal-Mogul', 'federal-mogul.com', '', ''
);

/* INSERT QUERY NO: 322 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
322, 'EMN', 'Eastman Chemical', 'eastman.com', '', ''
);

/* INSERT QUERY NO: 323 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
323, 'BHI', 'Baker Hughes', 'bakerhughes.com', '', ''
);

/* INSERT QUERY NO: 324 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
324, 'PHM', 'Pulte Homes', 'pulte.com', '', ''
);

/* INSERT QUERY NO: 325 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
325, 'AAPL', 'Apple Computer', 'apple.com', '', ''
);

/* INSERT QUERY NO: 326 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
326, 'DG', 'Dollar General', 'dollargeneral.com', '', ''
);

/* INSERT QUERY NO: 327 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
327, 'FO', 'Fortune Brands', 'fortunebrands.com', '', ''
);

/* INSERT QUERY NO: 328 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
328, 'RRD', 'R.R. Donnelley & Sons', 'rrdonnelley.com', '', ''
);

/* INSERT QUERY NO: 329 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
329, 'IAC', 'USA Networks', 'usanetwork.com', '', ''
);

/* INSERT QUERY NO: 330 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
330, 'SCHW', 'Charles Schwab', 'schwab.com', '', ''
);

/* INSERT QUERY NO: 331 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
331, 'BJ', 'BJ\'s Wholesale Club', 'bjs.com', '', ''
);

/* INSERT QUERY NO: 332 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
332, 'IKN', 'Ikon Office Solutions', 'ikon.com', '', ''
);

/* INSERT QUERY NO: 333 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
333, 'TRB', 'Tribune', 'tribune.com', '', ''
);

/* INSERT QUERY NO: 334 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
334, 'TLP', 'TransMontaigne', 'transmontaigne.com', '', ''
);

/* INSERT QUERY NO: 335 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
335, 'TSO', 'Tesoro Petroleum', 'tesoropetroleum.com', '', ''
);

/* INSERT QUERY NO: 336 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
336, 'PX', 'Praxair', 'praxair.com', '', ''
);

/* INSERT QUERY NO: 337 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
337, 'AFMIC', 'American Family Ins. Group', 'amfam.com', '', ''
);

/* INSERT QUERY NO: 338 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
338, 'EC', 'Engelhard', 'engelhard.com', '', ''
);

/* INSERT QUERY NO: 339 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
339, 'SHW', 'Sherwin-Williams', 'sherwin.com', '', ''
);

/* INSERT QUERY NO: 340 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
340, 'GR', 'Goodrich', 'bfgoodrich.com', '', ''
);

/* INSERT QUERY NO: 341 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
341, 'R', 'Ryder System', 'ryder.com', '', ''
);

/* INSERT QUERY NO: 342 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
342, 'CNF', 'CNF', 'cnf.com', '', ''
);

/* INSERT QUERY NO: 343 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
343, 'BKS', 'Barnes & Noble', 'barnesandnoble.com', '', ''
);

/* INSERT QUERY NO: 344 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
344, 'GRAYB', 'Graybar Electric', 'graybar.com', '', ''
);

/* INSERT QUERY NO: 345 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
345, 'CYWDF', 'Countrywide Credit Industries', 'countrywide.com', '', ''
);

/* INSERT QUERY NO: 346 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
346, 'AZO', 'AutoZone', 'autozone.com', '', ''
);

/* INSERT QUERY NO: 347 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
347, 'MAT', 'Mattel', 'mattel.com', '', ''
);

/* INSERT QUERY NO: 348 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
348, 'RSH', 'RadioShack', 'radioshackcorporation.com', '', ''
);

/* INSERT QUERY NO: 349 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
349, 'OC', 'Owens Corning', 'owenscorning.com', '', ''
);

/* INSERT QUERY NO: 350 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
350, 'GWW', 'W.W. Grainger', 'grainger.com', '', ''
);

/* INSERT QUERY NO: 351 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
351, 'AE', 'Adams Resources & Energy', '', '', ''
);

/* INSERT QUERY NO: 352 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
352, 'PBI', 'Pitney Bowes', 'pitneybowes.com', '', ''
);

/* INSERT QUERY NO: 353 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
353, 'DOLE', 'Dole Food', 'dole.com', '', ''
);

/* INSERT QUERY NO: 354 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
354, 'ITT', 'ITT Industries', 'itt.com', '', ''
);

/* INSERT QUERY NO: 355 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
355, 'KBH', 'KB Home', 'kbhome.com', '', ''
);

/* INSERT QUERY NO: 356 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
356, 'MHP', 'McGraw-Hill', 'mcgraw-hill.com', '', ''
);

/* INSERT QUERY NO: 357 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
357, 'OMX', 'OfficeMax', 'officemax.com', '', ''
);

/* INSERT QUERY NO: 358 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
358, 'CZR', 'Park Place Entertainment', 'parkplace.com', '', ''
);

/* INSERT QUERY NO: 359 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
359, 'SRP', 'Sierra Pacific Resources', 'sierrapacific.com', '', ''
);

/* INSERT QUERY NO: 360 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
360, 'EL', 'Estee Lauder', 'elcompanies.com', '', ''
);

/* INSERT QUERY NO: 361 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
361, 'MYG', 'Maytag', 'maytagcorp.com', '', ''
);

/* INSERT QUERY NO: 362 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
362, 'HSY', 'Hershey Foods', 'hersheys.com', '', ''
);

/* INSERT QUERY NO: 363 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
363, 'PNW', 'Pinnacle West Capital', 'pinnaclewest.com', '', ''
);

/* INSERT QUERY NO: 364 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
364, 'DOV', 'Dover', 'dovercorporation.com', '', ''
);

/* INSERT QUERY NO: 365 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
365, 'MU', 'Micron Technology', 'micron.com', '', ''
);

/* INSERT QUERY NO: 366 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
366, 'AEE', 'Ameren', 'ameren.com', '', ''
);

/* INSERT QUERY NO: 367 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
367, 'MUR', 'Murphy Oil', 'murphyoilcorp.com', '', ''
);

/* INSERT QUERY NO: 368 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
368, 'DHI', 'D.R. Horton', 'drhorton.com', '', ''
);

/* INSERT QUERY NO: 369 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
369, 'WI', 'Willamette Industries', 'weyerhaeuser.com', '', ''
);

/* INSERT QUERY NO: 370 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
370, 'QMCO', 'Quantum', 'quantum.com', '', ''
);

/* INSERT QUERY NO: 371 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
371, 'GDW', 'Golden West Financial', 'worldsavings.com', '', ''
);

/* INSERT QUERY NO: 372 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
372, 'OXHP', 'Oxford Health Plans', 'oxfordhealth.com', '', ''
);

/* INSERT QUERY NO: 373 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
373, 'CVC', 'Cablevision Systems', 'cablevision.com', '', ''
);

/* INSERT QUERY NO: 374 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
374, 'EHC', 'Healthsouth', 'healthsouth.com', '', ''
);

/* INSERT QUERY NO: 375 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
375, 'FL', 'Foot Locker', 'footlocker-inc.com', '', ''
);

/* INSERT QUERY NO: 376 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
376, 'NSP', 'Administaff', 'administaff.com', '', ''
);

/* INSERT QUERY NO: 377 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
377, 'BDK', 'Black & Decker', 'bdk.com', '', ''
);

/* INSERT QUERY NO: 378 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
378, 'JBL', 'Jabil Circuit', 'jabil.com', '', ''
);

/* INSERT QUERY NO: 379 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
379, '', 'Mutual of Omaha Insurance', 'mutualofomaha.com', '', ''
);

/* INSERT QUERY NO: 380 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
380, 'ROK', 'Rockwell Automation', 'rockwell.com', '', ''
);

/* INSERT QUERY NO: 381 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
381, 'GSBX', 'Golden State Bancorp', 'goldenstate.com', '', ''
);

/* INSERT QUERY NO: 382 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
382, 'LDG', 'Longs Drug Stores', 'longs.com', '', ''
);

/* INSERT QUERY NO: 383 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
383, 'LEVI', 'Levi Strauss', 'levistrauss.com', '', ''
);

/* INSERT QUERY NO: 384 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
384, 'KELYA', 'Kelly Services', 'kellyservices.com', '', ''
);

/* INSERT QUERY NO: 385 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
385, 'NWE', 'NorthWestern', 'northwestern.com', '', ''
);

/* INSERT QUERY NO: 386 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
386, 'CBE', 'Cooper Industries', 'cooperindustries.com', '', ''
);

/* INSERT QUERY NO: 387 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
387, 'CA', 'Computer Associates Intl.', 'ca.com', '', ''
);

/* INSERT QUERY NO: 388 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
388, 'CMA', 'Comerica', 'comerica.com', '', ''
);

/* INSERT QUERY NO: 389 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
389, 'TIN', 'Temple-Inland', 'temple-inland.com', '', ''
);

/* INSERT QUERY NO: 390 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
390, 'LXK', 'Lexmark International', 'lexmark.com', '', ''
);

/* INSERT QUERY NO: 391 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
391, 'NUE', 'Nucor', 'nucor.com', '', ''
);

/* INSERT QUERY NO: 392 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
392, 'HRL', 'Hormel Foods', 'hormel.com', '', ''
);

/* INSERT QUERY NO: 393 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
393, 'SPW', 'SPX', 'spx.com', '', ''
);

/* INSERT QUERY NO: 394 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
394, 'LEG', 'Leggett & Platt', 'leggett.com', '', ''
);

/* INSERT QUERY NO: 395 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
395, 'NAFC', 'Nash Finch', 'nashfinch.com', '', ''
);

/* INSERT QUERY NO: 396 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
396, 'JNY', 'Jones Apparel Group', 'jny.com', '', ''
);

/* INSERT QUERY NO: 397 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
397, 'COX', 'Cox Communications', 'cox.com', '', ''
);

/* INSERT QUERY NO: 398 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
398, 'BK', 'Mellon Financial Corp.', 'mellon.com', '', ''
);

/* INSERT QUERY NO: 399 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
399, 'SANM', 'Sanmina-SCI', 'sanmina.com', '', ''
);

/* INSERT QUERY NO: 400 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
400, 'RF', 'Regions Financial', 'regions.com', '', ''
);

/* INSERT QUERY NO: 401 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
401, 'DRI', 'Darden Restaurants', 'darden.com', '', ''
);

/* INSERT QUERY NO: 402 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
402, 'PTMK', 'Pathmark Stores', 'pathmark.com', '', ''
);

/* INSERT QUERY NO: 403 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
403, 'AMGN', 'Amgen', 'amgen.com', '', ''
);

/* INSERT QUERY NO: 404 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
404, 'MGM', 'MGM Mirage', 'mgmmirage.com', '', ''
);

/* INSERT QUERY NO: 405 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
405, 'BCO', 'Pittston', 'pittston.com', '', ''
);

/* INSERT QUERY NO: 406 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
406, 'PD', 'Phelps Dodge', 'phelpsdodge.com', '', ''
);

/* INSERT QUERY NO: 407 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
407, 'DISH', 'Echostar Communications', 'dishnetwork.com', '', ''
);

/* INSERT QUERY NO: 408 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
408, '', 'Group 1 Automotive', 'group1auto.com', '', ''
);

/* INSERT QUERY NO: 409 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
409, 'AKS', 'AK Steel Holding', 'aksteel.com', '', ''
);

/* INSERT QUERY NO: 410 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
410, 'ALV', 'Autoliv', 'autoliv.com', '', ''
);

/* INSERT QUERY NO: 411 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
411, 'MWV', 'MeadWestvaco', 'meadwestvaco.com', '', ''
);

/* INSERT QUERY NO: 412 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
412, 'EESI', 'Encompass Services', 'encompserv.com', '', ''
);

/* INSERT QUERY NO: 413 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
413, 'HOT', 'Starwood Hotels & Resorts', 'starwood.com', '', ''
);

/* INSERT QUERY NO: 414 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
414, 'CDW', 'CDW Computer Centers', 'cdw.com', '', ''
);

/* INSERT QUERY NO: 415 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
415, 'JEC', 'Jacobs Engineering Group', 'jacobs.com', '', ''
);

/* INSERT QUERY NO: 416 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
416, 'LTV', 'LTV', 'ltvsteel.com', '', ''
);

/* INSERT QUERY NO: 417 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
417, 'CHTR', 'Charter Communications', 'chartercom.com', '', ''
);

/* INSERT QUERY NO: 418 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
418, 'AFG', 'American Financial Group', 'amfnl.com', '', ''
);

/* INSERT QUERY NO: 419 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
419, 'YRK', 'York International', 'york.com', '', ''
);

/* INSERT QUERY NO: 420 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
420, 'WEC', 'Wisconsin Energy', 'wisenergy.com', '', ''
);

/* INSERT QUERY NO: 421 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
421, 'CEG', 'Constellation Energy', 'constellationenergy.com', '', ''
);

/* INSERT QUERY NO: 422 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
422, 'USTR', 'United Stationers', 'unitedstationers.com', '', ''
);

/* INSERT QUERY NO: 423 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
423, 'CLX', 'Clorox', 'clorox.com', '', ''
);

/* INSERT QUERY NO: 424 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
424, 'AMD', 'Advanced Micro Devices', 'amd.com', '', ''
);

/* INSERT QUERY NO: 425 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
425, 'SCS', 'Steelcase', 'steelcase.com', '', ''
);

/* INSERT QUERY NO: 426 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
426, 'FNF', 'Fidelity National Financial', 'fnf.com', '', ''
);

/* INSERT QUERY NO: 427 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
427, 'KIRY', 'Peter Kiewit Sons\'', 'kiewit.com', '', ''
);

/* INSERT QUERY NO: 428 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
428, 'FTI', 'FMC', 'fmc.com', '', ''
);

/* INSERT QUERY NO: 429 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
429, 'OMI', 'Owens & Minor', 'owens-minor.com', '', ''
);

/* INSERT QUERY NO: 430 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
430, 'AVY', 'Avery Dennison', 'averydennison.com', '', ''
);

/* INSERT QUERY NO: 431 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
431, 'MXO', 'MXO', 'maxtor.com', '', ''
);

/* INSERT QUERY NO: 432 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
432, 'DHR', 'Danaher', 'danaher.com', '', ''
);

/* INSERT QUERY NO: 433 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
433, 'EAS', 'Energy East', 'energyeast.com', '', ''
);

/* INSERT QUERY NO: 434 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
434, 'NTL', 'NTL', 'ntl.com', '', ''
);

/* INSERT QUERY NO: 435 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
435, 'BDX', 'Becton Dickinson', 'bd.com', '', ''
);

/* INSERT QUERY NO: 436 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
436, 'HMT', 'Host Marriott', 'hostmarriott.com', '', ''
);

/* INSERT QUERY NO: 437 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
437, 'FAF', 'First American Corp.', 'firstam.com', '', ''
);

/* INSERT QUERY NO: 438 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
438, 'SOTR', 'SouthTrust Corp.', 'southtrust.com', '', ''
);

/* INSERT QUERY NO: 439 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
439, '', 'Pacific LifeCorp', 'pacificlife.com', '', ''
);

/* INSERT QUERY NO: 440 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
440, 'HET', 'Harrah\'s Entertainment', 'harrahs.com', '', ''
);

/* INSERT QUERY NO: 441 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
441, 'BLL', 'Ball', 'ball.com', '', ''
);

/* INSERT QUERY NO: 442 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
442, 'BC', 'Brunswick', 'brunswick.com', '', ''
);

/* INSERT QUERY NO: 443 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
443, 'FDO', 'Family Dollar Stores', 'familydollar.com', '', ''
);

/* INSERT QUERY NO: 444 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
444, 'WCC', 'Wesco International', 'wescodist.com', '', ''
);

/* INSERT QUERY NO: 445 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
445, 'AMESQ', 'Ames Department Stores', 'amesstores.com', '', ''
);

/* INSERT QUERY NO: 446 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
446, 'KMG', 'Kerr-McGee', 'Kerr-McGee.com', '', ''
);

/* INSERT QUERY NO: 447 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
447, 'DGX', 'Quest Diagnostics', 'questdiagnostics.com', '', ''
);

/* INSERT QUERY NO: 448 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
448, 'SII', 'Smith International', 'smith.com', '', ''
);

/* INSERT QUERY NO: 449 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
449, 'SPTN', 'Spartan Stores', 'spartanstores.com', '', ''
);

/* INSERT QUERY NO: 450 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
450, 'NAVI', 'USA Education', 'salliemae.com', '', ''
);

/* INSERT QUERY NO: 451 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
451, 'IBCIQ', 'Interstate Bakeries', 'irin.com', '', ''
);

/* INSERT QUERY NO: 452 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
452, 'RNDY', 'Roundy\'s', 'roundys.com', '', ''
);

/* INSERT QUERY NO: 453 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
453, 'SCG', 'Scana', 'scana.com', '', ''
);

/* INSERT QUERY NO: 454 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
454, 'LIZ', 'Liz Claiborne', 'lizclaiborne.com', '', ''
);

/* INSERT QUERY NO: 455 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
455, 'MHK', 'Mohawk Industries', 'mohawkind.com', '', ''
);

/* INSERT QUERY NO: 456 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
456, 'ADELQ', 'Adelphia Communications', 'adelphia.net', '', ''
);

/* INSERT QUERY NO: 457 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
457, 'BLI', 'Big Lots', 'biglots.com', '', ''
);

/* INSERT QUERY NO: 458 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
458, 'CORE', 'Core-Mark International', 'coremark.com', '', ''
);

/* INSERT QUERY NO: 459 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
459, 'EME', 'Emcor Group', 'emcorgroup.com', '', ''
);

/* INSERT QUERY NO: 460 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
460, 'FWLT', 'Foster Wheeler', 'fwc.com', '', ''
);

/* INSERT QUERY NO: 461 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
461, 'BGP', 'Borders Group', 'bordersgroupinc.com', '', ''
);

/* INSERT QUERY NO: 462 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
462, 'SKO', 'Shopko Stores', 'shopko.com', '', ''
);

/* INSERT QUERY NO: 463 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
463, '', 'AmSouth Bancorp.', 'amsouth.com', '', ''
);

/* INSERT QUERY NO: 464 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
464, 'PSD', 'Puget Energy', 'pse.com', '', ''
);

/* INSERT QUERY NO: 465 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
465, 'TEN', 'Tenneco Automotive', 'tenneco-automotive.com', '', ''
);

/* INSERT QUERY NO: 466 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
466, 'HDI', 'Harley-Davidson', 'harley-davidson.com', '', ''
);

/* INSERT QUERY NO: 467 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
467, 'WGR', 'Western Gas Resources', 'westerngas.com', '', ''
);

/* INSERT QUERY NO: 468 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
468, 'BSC', 'Bethlehem Steel', 'bethsteel.com', '', ''
);

/* INSERT QUERY NO: 469 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
469, 'JP', 'Jefferson-Pilot', 'jpfinancial.com', '', ''
);

/* INSERT QUERY NO: 470 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
470, 'BR', 'Burlington Resources', 'br-inc.com', '', ''
);

/* INSERT QUERY NO: 471 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
471, 'THG', 'Allmerica Financial', 'allmerica.com', '', ''
);

/* INSERT QUERY NO: 472 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
472, 'USG', 'USG', 'usg.com', '', ''
);

/* INSERT QUERY NO: 473 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
473, 'AUSX', 'Yellow', 'yellowcorp.com', '', ''
);

/* INSERT QUERY NO: 474 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
474, 'NTRS', 'Northern Trust Corp.', 'northerntrust.com', '', ''
);

/* INSERT QUERY NO: 475 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
475, 'BAC', 'Aid Association for Lutherans', 'aal.org', '', ''
);

/* INSERT QUERY NO: 476 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
476, 'PFGC', 'Performance Food Group', 'pfgc.com', '', ''
);

/* INSERT QUERY NO: 477 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
477, 'JDSU', 'JDS Uniphase', 'jdsuniphase.com', '', ''
);

/* INSERT QUERY NO: 478 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
478, 'LYO', 'Lyondell Chemical', 'lyondell.com', '', ''
);

/* INSERT QUERY NO: 479 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
479, 'ABWN', 'Airborne', 'airborne.com', '', ''
);

/* INSERT QUERY NO: 480 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
480, 'CDCO', 'Comdisco', 'comdisco.com', '', ''
);

/* INSERT QUERY NO: 481 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
481, 'NST', 'NSTAR', 'nstaronline.com', '', ''
);

/* INSERT QUERY NO: 482 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
482, 'OGE', 'OGE Energy', 'oge.com', '', ''
);

/* INSERT QUERY NO: 483 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
483, 'STFF', 'Staff Leasing', 'gevityhr.com', '', ''
);

/* INSERT QUERY NO: 484 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
484, 'EPD', 'Enterprise Products', 'epplp.com', '', ''
);

/* INSERT QUERY NO: 485 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
485, 'PAS', 'PepsiAmericas', 'pepsiamericas.com', '', ''
);

/* INSERT QUERY NO: 486 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
486, 'CTB', 'Cooper Tire & Rubber', 'coopertire.com', '', ''
);

/* INSERT QUERY NO: 487 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
487, 'CVH', 'Coventry Health Care', 'coventryhealth.com', '', ''
);

/* INSERT QUERY NO: 488 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
488, 'AXE', 'Anixter International', 'anixter.com', '', ''
);

/* INSERT QUERY NO: 489 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
489, 'UPC', 'Union Planters Corp.', 'unionplanters.com', '', ''
);

/* INSERT QUERY NO: 490 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
490, 'AWI', 'Armstrong Holdings', 'armstrong.com', '', ''
);

/* INSERT QUERY NO: 491 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
491, 'EOP', 'Equity Office Properties', 'equityoffice.com', '', ''
);

/* INSERT QUERY NO: 492 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
492, 'AMZN', 'Amazon.Com', 'amazon.com', '', ''
);

/* INSERT QUERY NO: 493 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
493, 'LII', 'Lennox International', 'lennoxinternational.com', '', ''
);

/* INSERT QUERY NO: 494 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
494, 'AXL', 'American Axle & Mfg.', 'aam.com', '', ''
);

/* INSERT QUERY NO: 495 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
495, 'CHRW', 'C.H. Robinson Worldwide', 'chrobinson.com', '', ''
);

/* INSERT QUERY NO: 496 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
496, 'KND', 'Kindred Healthcare', 'kindredhealthcare.com', '', ''
);

/* INSERT QUERY NO: 497 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
497, 'DVN', 'Devon Energy', 'devonenergy.com', '', ''
);

/* INSERT QUERY NO: 498 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
498, 'SEE', 'Sealed Air', 'sealedair.com', '', ''
);

/* INSERT QUERY NO: 499 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
499, 'HLT', 'Hilton Hotels', 'hilton.com', '', ''
);

/* INSERT QUERY NO: 500 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
500, 'NYT', 'New York Times', 'nytco.com', '', ''
);

/* INSERT QUERY NO: 501 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
501, 'HUG', 'Hughes Supply', 'hughessupply.com', '', ''
);

/* INSERT QUERY NO: 502 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
502, 'VMC', 'Vulcan Materials', 'vulcanmaterials.com', '', ''
);

/* INSERT QUERY NO: 503 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
503, 'UHS', 'Universal', 'universalcorp.com', '', ''
);

/* INSERT QUERY NO: 504 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
504, '', 'Auto-Owners Insurance', 'auto-owners.com', '', ''
);

/* INSERT QUERY NO: 505 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
505, 'NMG.A', 'Neiman Marcus', 'neimanmarcus.com', '', ''
);

/* INSERT QUERY NO: 506 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
506, 'COMS', '3Com', '3com.com', '', ''
);

/* INSERT QUERY NO: 507 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
507, 'HRB', 'H&R Block', 'hrblock.com', '', ''
);

/* INSERT QUERY NO: 508 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
508, 'RBK', 'Reebok International', 'reebok.com', '', ''
);

/* INSERT QUERY NO: 509 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
509, 'ROST', 'Ross Stores', 'rossstores.com', '', ''
);

/* INSERT QUERY NO: 510 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
510, 'TGH', 'Trigon Healthcare', 'trigon.com', '', ''
);

/* INSERT QUERY NO: 511 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
511, '', 'Unified Western Grocers', 'uwgrocers.com', '', ''
);

/* INSERT QUERY NO: 512 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
512, 'PSS', 'Payless Shoesource', 'paylessshoesource.com', '', ''
);

/* INSERT QUERY NO: 513 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
513, '', 'TruServ', 'truserv.com', '', ''
);

/* INSERT QUERY NO: 514 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
514, 'AGYS', 'Pioneer-Standard Electronics', 'pios.com', '', ''
);

/* INSERT QUERY NO: 515 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
515, 'KRI', 'Knight-Ridder', 'kri.com', '', ''
);

/* INSERT QUERY NO: 516 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
516, '', 'Ace Hardware', 'acehardware.com', '', ''
);

/* INSERT QUERY NO: 517 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
517, 'URI', 'United Rentals', 'unitedrentals.com', '', ''
);

/* INSERT QUERY NO: 518 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
518, 'FSH', 'Fisher Scientific International', 'fisherscientific.com', '', ''
);

/* INSERT QUERY NO: 519 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
519, 'HAS', 'Hasbro', 'hasbro.com', '', ''
);

/* INSERT QUERY NO: 520 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
520, 'KCIN', 'KPMG Consulting', 'kpmgconsulting.com', '', ''
);

/* INSERT QUERY NO: 521 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
521, 'CFG', 'Charter One Financial', 'charterone.com', '', ''
);

/* INSERT QUERY NO: 522 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
522, 'TMO', 'Thermo Electron', 'thermo.com', '', ''
);

/* INSERT QUERY NO: 523 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
523, 'UHS', 'Universal Health Services', 'uhsinc.com', '', ''
);

/* INSERT QUERY NO: 524 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
524, 'AGE', 'A.G. Edwards', 'agedwards.com', '', ''
);

/* INSERT QUERY NO: 525 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
525, 'RIG', 'Transocean Sedco Forex', 'deepwater.com', '', ''
);

/* INSERT QUERY NO: 526 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
526, 'COL', 'Rockwell Collins', 'rockwellcollins.com', '', ''
);

/* INSERT QUERY NO: 527 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
527, 'SOA', 'Solutia', 'solutia.com', '', ''
);

/* INSERT QUERY NO: 528 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
528, 'PTV', 'Pactiv', 'pactiv.com', '', ''
);

/* INSERT QUERY NO: 529 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
529, 'WHC', 'Wackenhut', 'wackenhut.com', '', ''
);

/* INSERT QUERY NO: 530 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
530, 'PNR', 'Pentair', 'pentair.com', '', ''
);

/* INSERT QUERY NO: 531 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
531, 'ROAD', 'Roadway', 'roadway.com', '', ''
);

/* INSERT QUERY NO: 532 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
532, 'LNT', 'Alliant Energy', 'alliant-energy.com', '', ''
);

/* INSERT QUERY NO: 533 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
533, 'APA', 'Apache', 'apachecorp.com', '', ''
);

/* INSERT QUERY NO: 534 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
534, 'RDK', 'Ruddick', 'ruddickcorp.com', '', ''
);

/* INSERT QUERY NO: 535 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
535, 'RYL', 'Ryland Group', 'ryland.com', '', ''
);

/* INSERT QUERY NO: 536 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
536, 'CEM', 'Crompton', 'cromptoncorp.com', '', ''
);

/* INSERT QUERY NO: 537 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
537, 'V', 'Lutheran Brotherhood', 'aal.org', '', ''
);

/* INSERT QUERY NO: 538 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
538, 'IGL', 'IMC Global', 'imcglobal.com', '', ''
);

/* INSERT QUERY NO: 539 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
539, 'SFN', 'Spherion', 'spherion.com', '', ''
);

/* INSERT QUERY NO: 540 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
540, 'BEV', 'Beverly Enterprises', 'beverlynet.com', '', ''
);

/* INSERT QUERY NO: 541 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
541, 'MI', 'Marshall & Ilsley Corp.', 'micorp.com', '', ''
);

/* INSERT QUERY NO: 542 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
542, 'GDT', 'Guidant', 'guidant.com', '', ''
);

/* INSERT QUERY NO: 543 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
543, 'TMK', 'Torchmark', 'torchmarkcorp.com', '', ''
);

/* INSERT QUERY NO: 544 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
544, 'HCR', 'Manor Care', 'hcr-manorcare.com', '', ''
);

/* INSERT QUERY NO: 545 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
545, 'QCOM', 'Qualcomm', 'qualcomm.com', '', ''
);

/* INSERT QUERY NO: 546 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
546, 'WPS', 'WPS Resources', 'wpsr.com', '', ''
);

/* INSERT QUERY NO: 547 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
547, 'BSX', 'Boston Scientific', 'bsci.com', '', ''
);

/* INSERT QUERY NO: 548 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
548, 'TRI', 'Triad Hospitals', 'triadhospitals.com', '', ''
);

/* INSERT QUERY NO: 549 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
549, 'POL', 'PolyOne', 'polyone.com', '', ''
);

/* INSERT QUERY NO: 550 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
550, 'SBUX', 'Starbucks', 'starbucks.com', '', ''
);

/* INSERT QUERY NO: 551 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
551, 'TE', 'TECO Energy', 'tecoenergy.com', '', ''
);

/* INSERT QUERY NO: 552 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
552, 'SOV', 'Sovereign Bancorp', 'sovereignbank.com', '', ''
);

/* INSERT QUERY NO: 553 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
553, 'PTRY', 'Pantry', 'thepantry.com', '', ''
);

/* INSERT QUERY NO: 554 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
554, 'NC', 'Nacco Industries', 'nacco.com', '', ''
);

/* INSERT QUERY NO: 555 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
555, 'SWK', 'Stanley Works', 'stanleyworks.com', '', ''
);

/* INSERT QUERY NO: 556 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
556, 'NVR', 'NVR', 'nvrinc.com', '', ''
);

/* INSERT QUERY NO: 557 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
557, 'HPC', 'Hercules', 'herc.com', '', ''
);

/* INSERT QUERY NO: 558 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
558, 'SON', 'Sonoco Products', 'sonoco.com', '', ''
);

/* INSERT QUERY NO: 559 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
559, 'SYK', 'Stryker', 'strykercorp.com', '', ''
);

/* INSERT QUERY NO: 560 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
560, 'TDS', 'Telephone & Data Systems', 'teldta.com', '', ''
);

/* INSERT QUERY NO: 561 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
561, 'EGR', 'Earthgrains', 'saralee.com', '', ''
);

/* INSERT QUERY NO: 562 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
562, 'MTB', 'M & T Bank Corp.', 'mandtbank.com', '', ''
);

/* INSERT QUERY NO: 563 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
563, '', 'Stater Bros. Holdings', 'staterbros.com', '', ''
);

/* INSERT QUERY NO: 564 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
564, 'CZN', 'Citizens Communications', 'czn.com', '', ''
);

/* INSERT QUERY NO: 565 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
565, '', 'Genesis Health Ventures', 'ghv.com', '', ''
);

/* INSERT QUERY NO: 566 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
566, 'BPOP', 'Popular', 'bppr.com', '', ''
);

/* INSERT QUERY NO: 567 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
567, 'CINF', 'Cincinnati Financial', 'cinfin.com', '', ''
);

/* INSERT QUERY NO: 568 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
568, 'HSIC', 'Henry Schein', 'henryschein.com', '', ''
);

/* INSERT QUERY NO: 569 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
569, 'NSI', 'National Service Industries', 'nationalservice.com', '', ''
);

/* INSERT QUERY NO: 570 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
570, 'GAS', 'Nicor', 'nicor.com', '', ''
);

/* INSERT QUERY NO: 571 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
571, 'AG', 'AGCO', 'agcocorp.com', '', ''
);

/* INSERT QUERY NO: 572 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
572, 'UTR', 'Unitrin', 'unitrin.com', '', ''
);

/* INSERT QUERY NO: 573 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
573, 'FWD', 'Fleetwood Enterprises', 'fleetwood.com', '', ''
);

/* INSERT QUERY NO: 574 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
574, 'MIK', 'Michaels Stores', 'michaels.com', '', ''
);

/* INSERT QUERY NO: 575 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
575, '', 'International Multifoods', 'multifoods.com', '', ''
);

/* INSERT QUERY NO: 576 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
576, 'AM', 'American Greetings', 'americangreetings.com', '', ''
);

/* INSERT QUERY NO: 577 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
577, 'RDA', 'Reader\'s Digest Association', 'rd.com', '', ''
);

/* INSERT QUERY NO: 578 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
578, 'AAP', 'Advance Auto Parts', 'advance-auto.com', '', ''
);

/* INSERT QUERY NO: 579 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
579, 'SFA', 'SFA', 'scientificatlanta.com', '', ''
);

/* INSERT QUERY NO: 580 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
580, 'SFA', 'Service Corp. International', 'sci-corp.com', '', ''
);

/* INSERT QUERY NO: 581 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
581, 'POM', 'Potomac Electric Power', 'pepco.com', '', ''
);

/* INSERT QUERY NO: 582 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
582, 'PETM', 'PetsMart', 'petsmart.com', '', ''
);

/* INSERT QUERY NO: 583 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
583, 'ACV', 'Alberto-Culver', 'alberto.com', '', ''
);

/* INSERT QUERY NO: 584 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
584, 'PNFT', 'Penn Traffic', 'penntraffic.com', '', ''
);

/* INSERT QUERY NO: 585 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
585, 'DRRA', 'Dura Automotive Systems', 'duraauto.com', '', ''
);

/* INSERT QUERY NO: 586 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
586, 'EAT', 'Brinker International', 'brinker.com', '', ''
);

/* INSERT QUERY NO: 587 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
587, 'TSG', 'Sabre Holdings', 'sabre.com', '', ''
);

/* INSERT QUERY NO: 588 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
588, 'UGI', 'UGI', 'ugicorp.com', '', ''
);

/* INSERT QUERY NO: 589 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
589, 'TOWR', 'Tower Automotive', 'towerautomotive.com', '', ''
);

/* INSERT QUERY NO: 590 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
590, 'MBG', 'Mandalay Resort Group', 'mandalayresortgroup.com', '', ''
);

/* INSERT QUERY NO: 591 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
591, 'FTAR', 'Footstar', 'footstar.com', '', ''
);

/* INSERT QUERY NO: 592 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
592, '', 'USFreightways', 'usfreightways.com', '', ''
);

/* INSERT QUERY NO: 593 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
593, 'FHN', 'First Tennessee National Corp.', 'firsttennessee.com', '', ''
);

/* INSERT QUERY NO: 594 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
594, '', 'U.S. Industries', '', '', ''
);

/* INSERT QUERY NO: 595 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
595, 'RHI', 'Robert Half International', 'rhi.com', '', ''
);

/* INSERT QUERY NO: 596 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
596, 'BOW', 'Bowater', 'bowater.com', '', ''
);

/* INSERT QUERY NO: 597 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
597, 'HBAN', 'Huntington Bancshares', 'huntington.com', '', ''
);

/* INSERT QUERY NO: 598 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
598, 'TKR', 'Timken', 'timken.com', '', ''
);

/* INSERT QUERY NO: 599 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
599, 'CMC', 'Commercial Metals', 'commercialmetals.com', '', ''
);

/* INSERT QUERY NO: 600 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
600, 'CLHI', 'CellStar', 'cellstar.com', '', ''
);

/* INSERT QUERY NO: 601 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
601, 'XIDE', 'Exide Technologies', 'exideworld.com', '', ''
);

/* INSERT QUERY NO: 602 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
602, 'WWY', 'Wm. Wrigley Jr.', 'wrigley.com', '', ''
);

/* INSERT QUERY NO: 603 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
603, 'TAP', 'Adolph Coors', 'coors.com', '', ''
);

/* INSERT QUERY NO: 604 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
604, 'BCF', 'Burlington Coat Factory', 'coat.com', '', ''
);

/* INSERT QUERY NO: 605 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
605, 'PNX', 'Phoenix Companies', 'phoenixwm.com', '', ''
);

/* INSERT QUERY NO: 606 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
606, 'WPO', 'Washington Post', 'washingtonpost.com', '', ''
);

/* INSERT QUERY NO: 607 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
607, 'ADCT', 'ADC Telecommunications', 'adc.com', '', ''
);

/* INSERT QUERY NO: 608 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
608, 'STZ', 'Constellation Brands', 'cbrands.com', '', ''
);

/* INSERT QUERY NO: 609 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
609, 'BBBY', 'Bed Bath & Beyond', 'bedbathandbeyond.com', '', ''
);

/* INSERT QUERY NO: 610 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
610, '', 'Erie Insurance Group', 'erieinsurance.com', '', ''
);

/* INSERT QUERY NO: 611 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
611, 'WEN', 'Wendy\'s International', 'wendys.com', '', ''
);

/* INSERT QUERY NO: 612 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
612, 'ORI', 'Old Republic International', 'oldrepublic.com', '', ''
);

/* INSERT QUERY NO: 613 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
613, 'MKC', 'McCormick', 'mccormick.com', '', ''
);

/* INSERT QUERY NO: 614 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
614, 'COLT', 'OM Group', 'omgi.com', '', ''
);

/* INSERT QUERY NO: 615 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
615, 'MOLX', 'Molex', 'molex.com', '', ''
);

/* INSERT QUERY NO: 616 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
616, 'LPX', 'Louisiana-Pacific', 'lpcorp.com', '', ''
);

/* INSERT QUERY NO: 617 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
617, 'BEN', 'Franklin Resources', 'frk.com', '', ''
);

/* INSERT QUERY NO: 618 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
618, 'ECL', 'Ecolab', 'ecolab.com', '', ''
);

/* INSERT QUERY NO: 619 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
619, 'PNM', 'PNM Resources', 'pnm.com', '', ''
);

/* INSERT QUERY NO: 620 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
620, 'BWA', 'BorgWarner', 'bwauto.com', '', ''
);

/* INSERT QUERY NO: 621 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
621, 'BWNG', 'Broadwing', 'broadwing.com', '', ''
);

/* INSERT QUERY NO: 622 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
622, 'LLL', 'L-3 Communications', 'L-3com.com', '', ''
);

/* INSERT QUERY NO: 623 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
623, 'WFT', 'Weatherford International', 'weatherford.com', '', ''
);

/* INSERT QUERY NO: 624 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
624, 'PCP', 'Precision Castparts', 'precast.com', '', ''
);

/* INSERT QUERY NO: 625 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
625, 'CVG', 'Convergys', 'convergys.com', '', ''
);

/* INSERT QUERY NO: 626 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
626, 'URS', 'URS', 'urscorp.com', '', ''
);

/* INSERT QUERY NO: 627 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
627, 'PZL', 'Pennzoil-Quaker State', 'pennzoil-quakerstate.com', '', ''
);

/* INSERT QUERY NO: 628 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
628, 'VCF', 'Value City', 'valuecity.com', '', ''
);

/* INSERT QUERY NO: 629 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
629, 'BMS', 'Bemis', 'bemis.com', '', ''
);

/* INSERT QUERY NO: 630 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
630, '', 'Kellwood', 'kellwood.com', '', ''
);

/* INSERT QUERY NO: 631 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
631, 'BBBYLKIB', 'Belk', 'belk.com', '', ''
);

/* INSERT QUERY NO: 632 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
632, 'ADI', 'Analog Devices', 'analog.com', '', ''
);

/* INSERT QUERY NO: 633 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
633, 'WFMI', 'Whole Foods Market', 'wholefoods.com', '', ''
);

/* INSERT QUERY NO: 634 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
634, 'PGL', 'Peoples Energy', 'pecorp.com', '', ''
);

/* INSERT QUERY NO: 635 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
635, 'MAIL', 'Mail-Well', 'mail-well.com', '', ''
);

/* INSERT QUERY NO: 636 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
636, 'RSG', 'Republic Services', 'republicservices.com', '', ''
);

/* INSERT QUERY NO: 637 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
637, 'LZB', 'La-Z-Boy', 'lazboy.com', '', ''
);

/* INSERT QUERY NO: 638 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
638, 'RYI', 'Ryerson Tull', 'ryersontull.com', '', ''
);

/* INSERT QUERY NO: 639 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
639, 'CQB', 'Chiquita Brands International', 'chiquita.com', '', ''
);

/* INSERT QUERY NO: 640 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
640, 'CNF', 'Consolidated Freightways', 'cf.com', '', ''
);

/* INSERT QUERY NO: 641 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
641, 'MLHR', 'Herman Miller', 'hermanmiller.com', '', ''
);

/* INSERT QUERY NO: 642 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
642, 'CAR', 'Budget Group', 'drivebudget.com', '', ''
);

/* INSERT QUERY NO: 643 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
643, 'BJS', 'BJ Services', 'bjservices.com', '', ''
);

/* INSERT QUERY NO: 644 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
644, 'TOL', 'Toll Brothers', 'tollbrothers.com', '', ''
);

/* INSERT QUERY NO: 645 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
645, 'RL', 'Polo Ralph Lauren', 'polo.com', '', ''
);

/* INSERT QUERY NO: 646 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
646, 'NBR', 'Nabors Industries', 'nabors.com', '', ''
);

/* INSERT QUERY NO: 647 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
647, 'MDU', 'MDU Resources Group', 'mdu.com', '', ''
);

/* INSERT QUERY NO: 648 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
648, 'PPC', 'Pilgrim\'s Pride', 'pilgrimspride.com', '', ''
);

/* INSERT QUERY NO: 649 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
649, 'LH', 'Laboratory Corp. of America', 'labcorp.com', '', ''
);

/* INSERT QUERY NO: 650 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
650, 'TLAB', 'Tellabs', 'tellabs.com', '', ''
);

/* INSERT QUERY NO: 651 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
651, 'WRX', 'Western Resources', 'wr.com', '', ''
);

/* INSERT QUERY NO: 652 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
652, 'PBY', '"Pep Boys-Manny', ' Moe & Jack"', 'pepboys.com', ''
);

/* INSERT QUERY NO: 653 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
653, 'EQR', 'Equity Residential Properties', 'equityapartments.com', '', ''
);

/* INSERT QUERY NO: 654 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
654, 'LFG', 'LandAmerica Financial Group', 'landam.com', '', ''
);

/* INSERT QUERY NO: 655 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
655, 'VVC', 'Vectren', 'vectren.com', '', ''
);

/* INSERT QUERY NO: 656 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
656, 'CTAS', 'Cintas', 'cintas.com', '', ''
);

/* INSERT QUERY NO: 657 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
657, 'OCR', 'Omnicare', 'omnicare.com', '', ''
);

/* INSERT QUERY NO: 658 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
658, 'MAXX', 'Maxxam', '', '', ''
);

/* INSERT QUERY NO: 659 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
659, 'ALK', 'Alaska Air Group', 'alaskaair.com', '', ''
);

/* INSERT QUERY NO: 660 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
660, 'ANAT', 'American National Insurance', 'anico.com', '', ''
);

/* INSERT QUERY NO: 661 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
661, 'ATI', 'Allegheny Technologies', 'alleghenytechnologies.com', '', ''
);

/* INSERT QUERY NO: 662 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
662, 'OSI', 'Outback Steakhouse', 'outback.com', '', ''
);

/* INSERT QUERY NO: 663 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
663, 'MDC', 'MDC Holdings', 'richmondamerican.com', '', ''
);

/* INSERT QUERY NO: 664 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
664, 'SUNH', 'Sun Healthcare Group', 'sunh.com', '', ''
);

/* INSERT QUERY NO: 665 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
665, 'CTL', 'CenturyTel', 'centurytel.com', '', ''
);

/* INSERT QUERY NO: 666 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
666, 'NSM', 'National Semiconductor', 'national.com', '', ''
);

/* INSERT QUERY NO: 667 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
667, 'SWFT', 'Swift Transportation', 'swifttrans.com', '', ''
);

/* INSERT QUERY NO: 668 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
668, '', 'CUNA Mutual Group', 'cunamutual.com', '', ''
);

/* INSERT QUERY NO: 669 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
669, 'HSC', 'Harsco', 'harsco.com', '', ''
);

/* INSERT QUERY NO: 670 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
670, 'HB', 'Hillenbrand Industries', 'hillenbrand.com', '', ''
);

/* INSERT QUERY NO: 671 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
671, 'WYND', 'Wyndham International', 'wyndham.com', '', ''
);

/* INSERT QUERY NO: 672 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
672, 'KLAC', 'Kla-Tencor', 'kla-tencor.com', '', ''
);

/* INSERT QUERY NO: 673 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
673, 'MONY', 'MONY Group', 'mony.com', '', ''
);

/* INSERT QUERY NO: 674 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
674, 'NFG', 'National Fuel Gas', 'natfuel.com', '', ''
);

/* INSERT QUERY NO: 675 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
675, 'JBHT', 'J.B. Hunt Transport Services', 'jbhunt.com', '', ''
);

/* INSERT QUERY NO: 676 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
676, 'WSM', 'Williams-Sonoma', 'williams-sonomainc.com', '', ''
);

/* INSERT QUERY NO: 677 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
677, 'SNA', 'Snap-On', 'snapon.com', '', ''
);

/* INSERT QUERY NO: 678 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
678, '', 'Mariner Post-Acute Network', 'marinerhealth.com', '', ''
);

/* INSERT QUERY NO: 679 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
679, 'NSIT', 'Insight Enterprises', 'insight.com', '', ''
);

/* INSERT QUERY NO: 680 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
680, 'NTK', 'Nortek', 'nortek-inc.com', '', ''
);

/* INSERT QUERY NO: 681 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
681, 'PEO', 'PeopleSoft', 'peoplesoft.com', '', ''
);

/* INSERT QUERY NO: 682 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
682, 'SNV', 'Synovus Financial Corp.', 'synovus.com', '', ''
);

/* INSERT QUERY NO: 683 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
683, 'ZLC', 'Zale', 'zalecorp.com', '', ''
);

/* INSERT QUERY NO: 684 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
684, 'AWA', 'America West Holdings', 'americawest.com', '', ''
);

/* INSERT QUERY NO: 685 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
685, 'ACS', 'Affiliated Computer Services', 'acs-inc.com', '', ''
);

/* INSERT QUERY NO: 686 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
686, 'ETFC', 'E*Trade Group', 'etrade.com', '', ''
);

/* INSERT QUERY NO: 687 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
687, 'SPG', 'Simon Property Group', 'shopsimon.com', '', ''
);

/* INSERT QUERY NO: 688 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
688, 'NJR', 'New Jersey Resources', 'njresources.com', '', ''
);

/* INSERT QUERY NO: 689 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
689, 'SEBL', 'Siebel Systems', 'siebel.com', '', ''
);

/* INSERT QUERY NO: 690 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
690, 'STX', 'Storage Technology', 'storagetek.com', '', ''
);

/* INSERT QUERY NO: 691 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
691, 'PWR', 'Quanta Services', 'quantaservices.com', '', ''
);

/* INSERT QUERY NO: 692 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
692, 'ZION', 'Zions Bancorp.', 'zionsbancorporation.com', '', ''
);

/* INSERT QUERY NO: 693 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
693, 'CPWR', 'Compuware', 'compuware.com', '', ''
);

/* INSERT QUERY NO: 694 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
694, 'RPM', 'RPM', 'rpminc.com', '', ''
);

/* INSERT QUERY NO: 695 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
695, 'BELM', 'Bell Microproducts', 'bellmicro.com', '', ''
);

/* INSERT QUERY NO: 696 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
696, 'BGC', 'General Cable Corporation', 'generalcable.com', '', ''
);

/* INSERT QUERY NO: 697 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
697, 'VOLT', 'Volt Information Sciences', 'volt.com', '', ''
);

/* INSERT QUERY NO: 698 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
698, 'MPG', 'Metaldyne', 'metaldyne.com', '', ''
);

/* INSERT QUERY NO: 699 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
699, 'CHRS', 'Charming Shoppes', 'charmingshoppes.com', '', ''
);

/* INSERT QUERY NO: 700 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
700, 'WMK', 'Weis Markets', 'weismarkets.com', '', ''
);

/* INSERT QUERY NO: 701 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
701, 'DLTR', 'Dollar Tree Stores', 'dollartree.com', '', ''
);

/* INSERT QUERY NO: 702 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
702, 'BEC', 'Beckman Coulter', 'beckmancoulter.com', '', ''
);

/* INSERT QUERY NO: 703 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
703, 'PL', 'Protective Life', 'protective.com', '', ''
);

/* INSERT QUERY NO: 704 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
704, 'CBRL', 'CBRL Group', 'cbrlgroup.com', '', ''
);

/* INSERT QUERY NO: 705 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
705, 'SCHL', 'Scholastic', 'scholastic.com', '', ''
);

/* INSERT QUERY NO: 706 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
706, 'HRS', 'Harris', 'harris.com', '', ''
);

/* INSERT QUERY NO: 707 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
707, 'WDC', 'Western Digital', 'westerndigital.com', '', ''
);

/* INSERT QUERY NO: 708 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
708, 'IMKTA', 'Ingles Markets', 'ingles-markets.com', '', ''
);

/* INSERT QUERY NO: 709 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
709, 'ABM', 'ABM Industries', 'abm.com', '', ''
);

/* INSERT QUERY NO: 710 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
710, 'BER', 'W.R. Berkley', 'wrbc.com', '', ''
);

/* INSERT QUERY NO: 711 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
711, 'SLGN', 'Silgan Holdings', 'silgan.com', '', ''
);

/* INSERT QUERY NO: 712 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
712, 'WGL', 'WGL Holdings', 'wglholdings.com', '', ''
);

/* INSERT QUERY NO: 713 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
713, 'TA', 'TravelCenters of America', 'tatravelcenters.com', '', ''
);

/* INSERT QUERY NO: 714 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
714, 'SUG', 'Southern Union', 'southernunionco.com', '', ''
);

/* INSERT QUERY NO: 715 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
715, 'SDS', 'SunGard Data Systems', 'sungard.com', '', ''
);

/* INSERT QUERY NO: 716 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
716, 'CASY', 'Casey\'s General Stores', 'caseys.com', '', ''
);

/* INSERT QUERY NO: 717 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
717, 'SFE', 'Safeguard Scientifics', 'safeguard.com', '', ''
);

/* INSERT QUERY NO: 718 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
718, 'BF', 'Brown-Forman', 'brown-forman.com', '', ''
);

/* INSERT QUERY NO: 719 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
719, 'CH2M', 'CH2M Hill', 'ch2m.com', '', ''
);

/* INSERT QUERY NO: 720 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
720, 'WLT', 'Walter Industries', 'walterind.com', '', ''
);

/* INSERT QUERY NO: 721 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
721, 'VAL', 'Valspar', 'valspar.com', '', ''
);

/* INSERT QUERY NO: 722 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
722, 'FLS', 'Flowserve', 'flowserve.com', '', ''
);

/* INSERT QUERY NO: 723 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
723, 'TFX', 'Teleflex', 'teleflex.com', '', ''
);

/* INSERT QUERY NO: 724 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
724, 'TRN', 'Trinity Industries', 'trin.net', '', ''
);

/* INSERT QUERY NO: 725 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
725, 'OCAS', 'Ohio Casualty', 'ocas.com', '', ''
);

/* INSERT QUERY NO: 726 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
726, 'CBSS', 'Compass Bancshares', 'compassweb.com', '', ''
);

/* INSERT QUERY NO: 727 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
727, 'FBN', 'Furniture Brands International', 'furniturebrands.com', '', ''
);

/* INSERT QUERY NO: 728 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
728, 'FISV', 'Fiserv', 'fiserv.com', '', ''
);

/* INSERT QUERY NO: 729 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
729, '', 'Sentry Insurance Group', 'sentry.com', '', ''
);

/* INSERT QUERY NO: 730 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
730, 'DCP', 'DynCorp', 'dyncorp.com', '', ''
);

/* INSERT QUERY NO: 731 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
731, 'FTO', 'Frontier Oil', 'frontieroil.com', '', ''
);

/* INSERT QUERY NO: 732 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
732, 'APELY', 'Alpine Group', 'alpinegroup.com', '', ''
);

/* INSERT QUERY NO: 733 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
733, 'CPO', 'Corn Products International', 'cornproducts.com', '', ''
);

/* INSERT QUERY NO: 734 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
734, 'HMA', 'Health Management Associates', 'hma-corp.com', '', ''
);

/* INSERT QUERY NO: 735 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
735, 'MARSB', 'Marsh Supermarkets', 'marsh.net', '', ''
);

/* INSERT QUERY NO: 736 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
736, 'LAD', 'Lithia Motors', 'lithia.com', '', ''
);

/* INSERT QUERY NO: 737 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
737, 'MGLN', 'Magellan Health Services', 'magellanhealth.com', '', ''
);

/* INSERT QUERY NO: 738 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
738, 'SGI', 'Silicon Graphics', 'sgi.com', '', ''
);

/* INSERT QUERY NO: 739 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
739, 'MXT', 'Metris', 'metriscompanies.com', '', ''
);

/* INSERT QUERY NO: 740 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
740, 'CSL', 'Carlisle', 'carlisle.com', '', ''
);

/* INSERT QUERY NO: 741 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
741, 'LZ', 'Lubrizol', 'lubrizol.com', '', ''
);

/* INSERT QUERY NO: 742 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
742, '', 'Intl. Flavors & Fragrances', '', '', ''
);

/* INSERT QUERY NO: 743 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
743, 'FCX', 'Freeport-McMoRan Copper & Gold', 'fcx.com', '', ''
);

/* INSERT QUERY NO: 744 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
744, 'JBX', 'Jack in the Box', 'jackinthebox.com', '', ''
);

/* INSERT QUERY NO: 745 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
745, 'WOR', 'Worthington Industries', 'worthingtonindustries.com', '', ''
);

/* INSERT QUERY NO: 746 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
746, 'CELL', 'Brightpoint', 'brightpoint.com', '', ''
);

/* INSERT QUERY NO: 747 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
747, 'LIN', 'Linens\'n Things', 'lnt.com', '', ''
);

/* INSERT QUERY NO: 748 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
748, 'CKC', 'Collins & Aikman', 'collinsaikman.com', '', ''
);

/* INSERT QUERY NO: 749 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
749, 'PSSI', 'PSS World Medical', 'pssworldmedical.com', '', ''
);

/* INSERT QUERY NO: 750 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
750, 'UHAL', 'Amerco', 'uhaul.com', '', ''
);

/* INSERT QUERY NO: 751 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
751, 'TEX', 'Terex', 'terex.com', '', ''
);

/* INSERT QUERY NO: 752 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
752, 'MUSA', 'McLeodUSA', 'mcleodusa.com', '', ''
);

/* INSERT QUERY NO: 753 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
753, 'GKIS', 'Gold Kist', 'goldkist.com', '', ''
);

/* INSERT QUERY NO: 754 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
754, 'RCII', 'Rent A Center', 'rentacenter.com', '', ''
);

/* INSERT QUERY NO: 755 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
755, 'KMT', 'Kennametal', 'kennametal.com', '', ''
);

/* INSERT QUERY NO: 756 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
756, 'MME', 'Mid Atlantic Medical Services', 'mamsi.com', '', ''
);

/* INSERT QUERY NO: 757 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
757, 'BZH', 'Beazer Homes USA', 'beazer.com', '', ''
);

/* INSERT QUERY NO: 758 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
758, 'SEB', 'Seaboard', 'seaboardcorp.com', '', ''
);

/* INSERT QUERY NO: 759 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
759, '', 'Minnesota Life Ins.', 'minnesotamutual.com', '', ''
);

/* INSERT QUERY NO: 760 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
760, 'HNI', 'Hon Industries', 'honi.com', '', ''
);

/* INSERT QUERY NO: 761 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
761, 'PKG', 'Packaging Corp. of America', 'packagingcorp.com', '', ''
);

/* INSERT QUERY NO: 762 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
762, 'LSI', 'LSI Logic', 'lsilogic.com', '', ''
);

/* INSERT QUERY NO: 763 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
763, 'DJ', 'Dow Jones', 'dowjones.com', '', ''
);

/* INSERT QUERY NO: 764 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
764, 'WSPT', 'WestPoint Stevens', 'westpointstevens.com', '', ''
);

/* INSERT QUERY NO: 765 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
765, 'EQT', 'Equitable Resources', 'eqt.com', '', ''
);

/* INSERT QUERY NO: 766 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
766, 'DBD', 'Diebold', 'diebold.com', '', ''
);

/* INSERT QUERY NO: 767 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
767, 'GRA', 'W.R. Grace', 'grace.com', '', ''
);

/* INSERT QUERY NO: 768 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
768, 'CAL', 'Brown Shoe', 'brownshoe.com', '', ''
);

/* INSERT QUERY NO: 769 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
769, 'SEQUA', 'Sequa', 'sequa.com', '', ''
);

/* INSERT QUERY NO: 770 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
770, 'PCH', 'Potlatch', 'potlatchcorp.com', '', ''
);

/* INSERT QUERY NO: 771 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
771, 'SMG', 'Scotts Company', 'scotts.com', '', ''
);

/* INSERT QUERY NO: 772 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
772, 'NOV', 'National Oilwell', 'natoil.com', '', ''
);

/* INSERT QUERY NO: 773 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
773, 'PRM', 'Primedia', 'primediainc.com', '', ''
);

/* INSERT QUERY NO: 774 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
774, 'HOV', 'Hovnanian Enterprises', 'khov.com', '', ''
);

/* INSERT QUERY NO: 775 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
775, 'SOST', 'Southern States Coop.', 'southernstates.com', '', ''
);

/* INSERT QUERY NO: 776 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
776, 'PAYX', 'Paychex', 'paychex.com', '', ''
);

/* INSERT QUERY NO: 777 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
777, 'HE', 'Hawaiian Electric Industries', 'hei.com', '', ''
);

/* INSERT QUERY NO: 778 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
778, 'NFB', 'Greenpoint Financial', 'greenpoint.com', '', ''
);

/* INSERT QUERY NO: 779 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
779, 'HAR', 'Harman Intl. Industries', 'harman.com', '', ''
);

/* INSERT QUERY NO: 780 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
780, 'BOL', 'Bausch & Lomb', 'bausch.com', '', ''
);

/* INSERT QUERY NO: 781 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
781, 'CE', 'Concord EFS', 'concordefs.com', '', ''
);

/* INSERT QUERY NO: 782 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
782, 'COG', 'Cabot', 'cabot-corp.com', '', ''
);

/* INSERT QUERY NO: 783 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
783, 'DIAL', 'Dial', 'dialcorp.com', '', ''
);

/* INSERT QUERY NO: 784 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
784, 'ENR', 'Energizer Holdings', 'energizer.com', '', ''
);

/* INSERT QUERY NO: 785 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
785, 'CYH', 'Community Health Systems', 'chs.net', '', ''
);

/* INSERT QUERY NO: 786 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
786, 'IESC', 'Integrated Electrical Services', 'ielectric.com', '', ''
);

/* INSERT QUERY NO: 787 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
787, 'WCS', 'Wallace Computer Services', 'wallace.com', '', ''
);

/* INSERT QUERY NO: 788 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
788, 'AGN', 'Allergan', 'allergan.com', '', ''
);

/* INSERT QUERY NO: 789 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
789, 'MMX', 'Metals USA', 'metalsusa.com', '', ''
);

/* INSERT QUERY NO: 790 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
790, 'EAGL', 'EGL', 'eagleusa.com', '', ''
);

/* INSERT QUERY NO: 791 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
791, 'ALE', 'Allete', 'allete.com', '', ''
);

/* INSERT QUERY NO: 792 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
792, 'RS', 'Reliance Steel & Aluminum', 'rsac.com', '', ''
);

/* INSERT QUERY NO: 793 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
793, 'DST', 'DST Systems', 'dstsystems.com', '', ''
);

/* INSERT QUERY NO: 794 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
794, 'VVI', 'Viad', 'viad.com', '', ''
);

/* INSERT QUERY NO: 795 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
795, 'XLNX', 'Xilinx', 'xilinx.com', '', ''
);

/* INSERT QUERY NO: 796 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
796, 'RJF', 'Raymond James Financial', 'rjf.com', '', ''
);

/* INSERT QUERY NO: 797 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
797, 'NEM', 'Newmont Mining', 'newmont.com', '', ''
);

/* INSERT QUERY NO: 798 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
798, 'VSH', 'Vishay Intertechnology', 'vishay.com', '', ''
);

/* INSERT QUERY NO: 799 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
799, 'EOG', 'EOG Resources', 'eogresources.com', '', ''
);

/* INSERT QUERY NO: 800 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
800, '', 'Expeditors Intl of Washington', 'expeditors.com', '', ''
);

/* INSERT QUERY NO: 801 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
801, 'DVA', 'DaVita', 'davita.com', '', ''
);

/* INSERT QUERY NO: 802 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
802, 'DKHR', 'D&K Healthcare Resources', 'dkwd.com', '', ''
);

/* INSERT QUERY NO: 803 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
803, 'ABI', 'Applera', 'applera.com', '', ''
);

/* INSERT QUERY NO: 804 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
804, 'UST', 'UST', 'ustinc.com', '', ''
);

/* INSERT QUERY NO: 805 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
805, 'FLO', 'Flowers Foods', 'flowersfoods.com', '', ''
);

/* INSERT QUERY NO: 806 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
806, 'ARG', 'Airgas', 'airgas.com', '', ''
);

/* INSERT QUERY NO: 807 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
807, 'AIT', 'Applied Industrial Technologies', 'appliedindustrial.com', '', ''
);

/* INSERT QUERY NO: 808 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
808, 'IQV', 'Quintiles Transnational', 'quintiles.com', '', ''
);

/* INSERT QUERY NO: 809 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
809, 'TIF', 'Tiffany & Co', 'tiffany.com', '', ''
);

/* INSERT QUERY NO: 810 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
810, 'CIEN', 'Ciena', 'ciena.com', '', ''
);

/* INSERT QUERY NO: 811 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
811, 'PKI', 'PerkinElmer', 'perkinelmer.com', '', ''
);

/* INSERT QUERY NO: 812 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
812, 'GLK', 'Great Lakes Chemical', 'greatlakeschem.com', '', ''
);

/* INSERT QUERY NO: 813 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
813, 'MCH', 'Millennium Chemicals', 'millenniumchem.com', '', ''
);

/* INSERT QUERY NO: 814 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
814, 'CR', 'Crane', 'craneco.com', '', ''
);

/* INSERT QUERY NO: 815 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
815, 'SFG', 'StanCorp Financial', 'stancorpfinancial.com', '', ''
);

/* INSERT QUERY NO: 816 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
816, 'MXIM', 'Maxim Integrated Products', 'maxim-ic.com', '', ''
);

/* INSERT QUERY NO: 817 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
817, '', 'Agway', 'agway.com', '', ''
);

/* INSERT QUERY NO: 818 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
818, 'NBL', 'Noble Affiliates', 'nobleaff.com', '', ''
);

/* INSERT QUERY NO: 819 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
819, 'JAS', 'Jo Ann Stores', 'joann.com', '', ''
);

/* INSERT QUERY NO: 820 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
820, 'LE', 'Lands\' End', 'landsend.com', '', ''
);

/* INSERT QUERY NO: 821 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
821, 'CAM', 'Cooper Cameron', 'coopercameron.com', '', ''
);

/* INSERT QUERY NO: 822 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
822, 'BKH', 'Black Hills', 'blackhillscorporation.com', '', ''
);

/* INSERT QUERY NO: 823 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
823, 'JNS', 'Stilwell Financial', 'stilwellfinancial.com', '', ''
);

/* INSERT QUERY NO: 824 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
824, '', 'Perini', 'perini.com', '', ''
);

/* INSERT QUERY NO: 825 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
825, 'TNB', 'Thomas & Betts', 'tnb.com', '', ''
);

/* INSERT QUERY NO: 826 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
826, 'IPSU', 'Imperial Sugar', 'imperialsugar.com', '', ''
);

/* INSERT QUERY NO: 827 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
827, 'MBB', 'MPS Group', 'mpsgroup.com', '', ''
);

/* INSERT QUERY NO: 828 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
828, 'CHB', 'Champion Enterprises', 'championhomes.net', '', ''
);

/* INSERT QUERY NO: 829 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
829, 'GVA', 'Granite', 'graniteconstruction.com', '', ''
);

/* INSERT QUERY NO: 830 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
830, 'NCOM', 'National Commerce Financial', 'ncfcorp.com', '', ''
);

/* INSERT QUERY NO: 831 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
831, 'SYX', 'Systemax', 'systemax.com', '', ''
);

/* INSERT QUERY NO: 832 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
832, 'FIX', 'Comfort Systems USA', 'comfortsystemsusa.com', '', ''
);

/* INSERT QUERY NO: 833 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
833, 'GEF', 'Greif Bros.', 'greif.com', '', ''
);

/* INSERT QUERY NO: 834 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
834, 'AF', 'Astoria Financial', 'astoriafederal.com', '', ''
);

/* INSERT QUERY NO: 835 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
835, 'SGR', 'Shaw Group', 'shawgrp.com', '', ''
);

/* INSERT QUERY NO: 836 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
836, '', 'Di Giorgio', 'whiterose.com', '', ''
);

/* INSERT QUERY NO: 837 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
837, 'EFX', 'Equifax', 'equifax.com', '', ''
);

/* INSERT QUERY NO: 838 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
838, 'LM', 'Legg Mason', 'leggmason.com', '', ''
);

/* INSERT QUERY NO: 839 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
839, 'ACTT', 'ACT Manufacturing', 'actmfg.com', '', ''
);

/* INSERT QUERY NO: 840 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
840, 'LVLT', 'Level 3 Communications', 'level3.com', '', ''
);

/* INSERT QUERY NO: 841 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
841, 'RGSEQ', 'RGS Energy Group', 'rge.com', '', ''
);

/* INSERT QUERY NO: 842 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
842, 'UFPI', 'Universal Forest Products', 'ufpi.com', '', ''
);

/* INSERT QUERY NO: 843 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
843, 'INT', 'World Fuel Services', 'worldfuel.com', '', ''
);

/* INSERT QUERY NO: 844 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
844, 'IN', 'Unova', 'unova.com', '', ''
);

/* INSERT QUERY NO: 845 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
845, 'ARCB', 'Arkansas Best', 'arkbest.com', '', ''
);

/* INSERT QUERY NO: 846 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
846, 'GMT', 'GATX', 'gatx.com', '', ''
);

/* INSERT QUERY NO: 847 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
847, 'LRCX', 'Lam Research', 'lamrc.com', '', ''
);

/* INSERT QUERY NO: 848 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
848, 'AMKR', 'Amkor Technology', 'amkor.com', '', ''
);

/* INSERT QUERY NO: 849 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
849, 'PDE', 'Pride International', 'prideinternational.com', '', ''
);

/* INSERT QUERY NO: 850 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
850, 'PII', 'Polaris Industries', 'polarisindustries.com', '', ''
);

/* INSERT QUERY NO: 851 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
851, 'DLM', 'Del Monte Foods', 'delmonte.com', '', ''
);

/* INSERT QUERY NO: 852 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
852, 'MCY', 'Mercury General', 'mercuryinsurance.com', '', ''
);

/* INSERT QUERY NO: 853 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
853, 'MLM', 'Martin Marietta Materials', 'martinmarietta.com', '', ''
);

/* INSERT QUERY NO: 854 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
854, 'USON', 'US Oncology', 'usoncology.com', '', ''
);

/* INSERT QUERY NO: 855 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
855, 'TD', 'Banknorth Group', 'banknorth.com', '', ''
);

/* INSERT QUERY NO: 856 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
856, 'BMCS', 'BMC Software', 'bmc.com', '', ''
);

/* INSERT QUERY NO: 857 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
857, 'FOE', 'Ferro', 'ferro.com', '', ''
);

/* INSERT QUERY NO: 858 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
858, 'VRTSE', 'Veritas Software', 'veritas.com', '', ''
);

/* INSERT QUERY NO: 859 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
859, 'ACI', 'Arch Coal', 'archcoal.com', '', ''
);

/* INSERT QUERY NO: 860 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
860, 'CDI', 'CDI', 'cdicorp.com', '', ''
);

/* INSERT QUERY NO: 861 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
861, 'AJRD', 'GenCorp', 'gencorp.com', '', ''
);

/* INSERT QUERY NO: 862 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
862, '', 'Hibernia Corp.', 'hibernia.com', '', ''
);

/* INSERT QUERY NO: 863 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
863, 'SIE', 'Sierra Health Services', 'sierrahealth.com', '', ''
);

/* INSERT QUERY NO: 864 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
864, 'ATML', 'Atmel', 'atmel.com', '', ''
);

/* INSERT QUERY NO: 865 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
865, 'AIV', 'AIMCO', 'aimco.com', '', ''
);

/* INSERT QUERY NO: 866 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
866, 'GXP', 'Great Plains Energy', 'gpenergy.com', '', ''
);

/* INSERT QUERY NO: 867 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
867, 'SSP', 'E.W. Scripps', 'scripps.com', '', ''
);

/* INSERT QUERY NO: 868 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
868, 'BN', 'Banta', 'banta.com', '', ''
);

/* INSERT QUERY NO: 869 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
869, 'SBL', 'Symbol Technologies', 'symbol.com', '', ''
);

/* INSERT QUERY NO: 870 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
870, 'MNST', 'TMP Worldwide', 'tmp.com', '', ''
);

/* INSERT QUERY NO: 871 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
871, 'OSK', 'Oshkosh Truck', 'oshkoshtruck.com', '', ''
);

/* INSERT QUERY NO: 872 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
872, 'UNS', 'Unisource Energy', 'unisourceenergy.com', '', ''
);

/* INSERT QUERY NO: 873 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
873, 'ATO', 'Atmos Energy', 'atmosenergy.com', '', ''
);

/* INSERT QUERY NO: 874 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
874, 'RKT', 'Rock-Tenn', 'rocktenn.com', '', ''
);

/* INSERT QUERY NO: 875 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
875, 'TER', 'Teradyne', 'teradyne.com', '', ''
);

/* INSERT QUERY NO: 876 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
876, 'STR', 'Questar', 'questarcorp.com', '', ''
);

/* INSERT QUERY NO: 877 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
877, 'AWK', 'American Water Works', 'amwater.com', '', ''
);

/* INSERT QUERY NO: 878 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
878, 'CAO', 'CSK Auto', 'cskauto.com', '', ''
);

/* INSERT QUERY NO: 879 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
879, 'CKR', 'CKE Restaurants', 'ckr.com', '', ''
);

/* INSERT QUERY NO: 880 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
880, 'APCC', 'American Power Conversion', 'apcc.com', '', ''
);

/* INSERT QUERY NO: 881 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
881, 'PVH', 'Phillips-Van Heusen', 'pvh.com', '', ''
);

/* INSERT QUERY NO: 882 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
882, 'CDNS', 'Cadence Design Systems', 'cadence.com', '', ''
);

/* INSERT QUERY NO: 883 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
883, 'TSA', 'Sports Authority', 'sportsauthority.com', '', ''
);

/* INSERT QUERY NO: 884 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
884, 'PIRRQ', 'Pier 1 Imports', 'pier1.com', '', ''
);

/* INSERT QUERY NO: 885 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
885, 'FCS', 'Fairchild Semiconductor Intl.', 'fairchildsemi.com', '', ''
);

/* INSERT QUERY NO: 886 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
886, 'KEM', 'Kemet', 'kemet.com', '', ''
);

/* INSERT QUERY NO: 887 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
887, 'BURL', 'Burlington Industries', 'burlington.com', '', ''
);

/* INSERT QUERY NO: 888 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
888, 'DRYR', 'Dreyer\'s Grand Ice Cream', 'dreyersinc.com', '', ''
);

/* INSERT QUERY NO: 889 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
889, 'DIAM', 'Dimon', 'dimon.com', '', ''
);

/* INSERT QUERY NO: 890 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
890, 'SVC', 'Stewart & Stevenson Services', 'ssss.com', '', ''
);

/* INSERT QUERY NO: 891 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
891, 'TECU', 'Tecumseh Products', 'tecumseh.com', '', ''
);

/* INSERT QUERY NO: 892 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
892, 'MKL', 'Markel', 'markelcorp.com', '', ''
);

/* INSERT QUERY NO: 893 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
893, 'SWX', 'Southwest Gas', 'swgas.com', '', ''
);

/* INSERT QUERY NO: 894 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
894, 'LSTR', 'Landstar System', 'landstar.com', '', ''
);

/* INSERT QUERY NO: 895 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
895, '', 'Advantica', 'advantica-dine.com', '', ''
);

/* INSERT QUERY NO: 896 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
896, 'NRUC', 'National Rural Utilities Cooperative', 'nrucfc.org', '', ''
);

/* INSERT QUERY NO: 897 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
897, 'TWMC', 'Trans World Entertainment', 'twec.com', '', ''
);

/* INSERT QUERY NO: 898 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
898, 'MGM', 'Metro-Goldwyn-Mayer', 'mgm.com', '', ''
);

/* INSERT QUERY NO: 899 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
899, 'CYT', 'Cytec Industries', 'cytec.com', '', ''
);

/* INSERT QUERY NO: 900 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
900, 'SPF', 'Standard Pacific', 'standardpacifichomes.com', '', ''
);

/* INSERT QUERY NO: 901 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
901, 'HKYW', 'Hollywood Entertainment', 'hollywoodvideo.com', '', ''
);

/* INSERT QUERY NO: 902 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
902, 'KND', 'Gentiva Health Services', 'gentiva.com', '', ''
);

/* INSERT QUERY NO: 903 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
903, 'AEOS', 'American Eagle Outfitters', 'ae.com', '', ''
);

/* INSERT QUERY NO: 904 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
904, '', 'OneAmerica Financial', 'aul.com', '', ''
);

/* INSERT QUERY NO: 905 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
905, 'NVDA', 'Nvidia', 'nvidia.com', '', ''
);

/* INSERT QUERY NO: 906 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
906, 'GMST', 'Gemstar-TV Guide International', 'gemstartvguide.com', '', ''
);

/* INSERT QUERY NO: 907 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
907, 'ACTR', 'Acterna', 'acterna.com', '', ''
);

/* INSERT QUERY NO: 908 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
908, 'ANF', 'Abercrombie & Fitch', 'abercrombie.com', '', ''
);

/* INSERT QUERY NO: 909 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
909, 'BLC', 'Belo', 'belo.com', '', ''
);

/* INSERT QUERY NO: 910 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
910, 'MTG', 'MGIC Investment', 'mgic.com', '', ''
);

/* INSERT QUERY NO: 911 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
911, 'TTC', 'Toro', 'toro.com', '', ''
);

/* INSERT QUERY NO: 912 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
912, 'KCIIX', 'Knights of Columbus', 'kofc.org', '', ''
);

/* INSERT QUERY NO: 913 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
913, 'STJ', 'St. Jude Medical', 'sjm.com', '', ''
);

/* INSERT QUERY NO: 914 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
914, 'NVLS', 'Novellus Systems', 'novellus.com', '', ''
);

/* INSERT QUERY NO: 915 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
915, 'PFACP', 'Pro-Fac Cooperative', 'agrilinkfoods.com', '', ''
);

/* INSERT QUERY NO: 916 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
916, 'PFG', 'Provident Financial Group', 'providentbank.com', '', ''
);

/* INSERT QUERY NO: 917 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
917, 'RX', 'IMS Health', 'imshealth.com', '', ''
);

/* INSERT QUERY NO: 918 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
918, '', 'Gentek', 'gentek-global.com', '', ''
);

/* INSERT QUERY NO: 919 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
919, 'CIT', 'IT Group', 'theitgroup.com', '', ''
);

/* INSERT QUERY NO: 920 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
920, 'CRS', 'Carpenter Technology', 'cartech.com', '', ''
);

/* INSERT QUERY NO: 921 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
921, 'ERTS', 'Electronic Arts', 'ea.com', '', ''
);

/* INSERT QUERY NO: 922 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
922, 'REV', 'Revlon', 'revlon.com', '', ''
);

/* INSERT QUERY NO: 923 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
923, 'SMRT', 'Stein Mart', 'steinmart.com', '', ''
);

/* INSERT QUERY NO: 924 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
924, 'HUBG', 'Hub Group', 'hubgroup.com', '', ''
);

/* INSERT QUERY NO: 925 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
925, 'UDI', 'United Defense Industries', 'uniteddefense.com', '', ''
);

/* INSERT QUERY NO: 926 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
926, 'BGG', 'Briggs & Stratton', 'briggsandstratton.com', '', ''
);

/* INSERT QUERY NO: 927 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
927, 'HUB/B', 'Hubbell', 'hubbell.com', '', ''
);

/* INSERT QUERY NO: 928 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
928, 'RGS', 'Regis', 'regiscorp.com', '', ''
);

/* INSERT QUERY NO: 929 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
929, 'DNB', 'Dun & Bradstreet', 'dnb.com', '', ''
);

/* INSERT QUERY NO: 930 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
930, 'PETC', 'Petco Animal Supplies', 'petco.com', '', ''
);

/* INSERT QUERY NO: 931 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
931, '', 'Federated Mutual Insurance', 'federatedinsurance.com', '', ''
);

/* INSERT QUERY NO: 932 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
932, 'ANN', 'AnnTaylor', 'anntaylor.com', '', ''
);

/* INSERT QUERY NO: 933 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
933, 'FINN', 'First National of Nebraska', 'firstnational.com', '', ''
);

/* INSERT QUERY NO: 934 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
934, 'DQE', 'DQE', 'dqe.com', '', ''
);

/* INSERT QUERY NO: 935 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
935, 'BOH', 'Pacific Century Financial', 'boh.com', '', ''
);

/* INSERT QUERY NO: 936 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
936, 'DLX', 'Deluxe', 'dlx.com', '', ''
);

/* INSERT QUERY NO: 937 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
937, 'BHE', 'Benchmark Electronics', 'bench.com', '', ''
);

/* INSERT QUERY NO: 938 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
938, '', 'Amtran', 'ata.com', '', ''
);

/* INSERT QUERY NO: 939 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
939, 'FUL', 'H.B. Fuller', 'hbfuller.com', '', ''
);

/* INSERT QUERY NO: 940 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
940, 'MW', 'Men\'s Wearhouse', 'menswearhouse.com', '', ''
);

/* INSERT QUERY NO: 941 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
941, 'STC', 'Stewart Information Services', 'stewart.com', '', ''
);

/* INSERT QUERY NO: 942 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
942, 'OLN', 'Olin', 'olin.com', '', ''
);

/* INSERT QUERY NO: 943 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
943, 'WERN', 'Werner Enterprises', 'werner.com', '', ''
);

/* INSERT QUERY NO: 944 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
944, 'CMVT', 'Comverse Technology', 'cmvt.com', '', ''
);

/* INSERT QUERY NO: 945 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
945, 'NOV', 'Varco International', 'varco.com', '', ''
);

/* INSERT QUERY NO: 946 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
946, 'VOXX', 'Audiovox', 'audiovox.com', '', ''
);

/* INSERT QUERY NO: 947 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
947, 'CSCO', 'Amica Mutual Insurance', 'amica.com', '', ''
);

/* INSERT QUERY NO: 948 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
948, 'MCRN', 'Milacron', 'milacron.com', '', ''
);

/* INSERT QUERY NO: 949 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
949, 'ISRG', 'Intuit', 'intuit.com', '', ''
);

/* INSERT QUERY NO: 950 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
950, 'KBAL', 'Kimball International', 'kimball.com', '', ''
);

/* INSERT QUERY NO: 951 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
951, 'XOXO', 'XO Communications', 'xo.com', '', ''
);

/* INSERT QUERY NO: 952 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
952, 'DPZ', 'Domino\'s', 'dominos.com', '', ''
);

/* INSERT QUERY NO: 953 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
953, 'CPWR', 'Ocean Energy', 'oceanenergy.com', '', ''
);

/* INSERT QUERY NO: 954 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
954, 'MEE', 'Massey Energy', 'masseyenergyco.com', '', ''
);

/* INSERT QUERY NO: 955 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
955, 'TXI', 'Texas Industries', 'txi.com', '', ''
);

/* INSERT QUERY NO: 956 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
956, '', 'Riverwood Holding', 'riverwood.com', '', ''
);

/* INSERT QUERY NO: 957 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
957, 'ELNK', 'EarthLink', 'earthlink.net', '', ''
);

/* INSERT QUERY NO: 958 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
958, 'CEN', 'Ceridian', 'ceridian.com', '', ''
);

/* INSERT QUERY NO: 959 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
959, '', 'Union Central Life', 'unioncentral.com', '', ''
);

/* INSERT QUERY NO: 960 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
960, 'PMORQ', 'Phar-Mor', 'pharmor.com', '', ''
);

/* INSERT QUERY NO: 961 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
961, 'WSO', 'Watsco', 'watsco.com', '', ''
);

/* INSERT QUERY NO: 962 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
962, 'ENVA', 'Foamex International', 'foamex.com', '', ''
);

/* INSERT QUERY NO: 963 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
963, 'CMGI', 'CMGI', 'cmgi.com', '', ''
);

/* INSERT QUERY NO: 964 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
964, 'PLL', 'Pall', 'pall.com', '', ''
);

/* INSERT QUERY NO: 965 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
965, 'HARL', 'Harleysville Mutual Insurance', 'harleysvillegroup.com', '', ''
);

/* INSERT QUERY NO: 966 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
966, 'TEK', 'Tektronix', 'tek.com', '', ''
);

/* INSERT QUERY NO: 967 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
967, '', 'Oglethorpe Power', 'opc.com', '', ''
);

/* INSERT QUERY NO: 968 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
968, 'IDT', 'IDT', 'idt.net', '', ''
);

/* INSERT QUERY NO: 969 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
969, 'ADBE', 'Adobe Systems', 'adobe.com', '', ''
);

/* INSERT QUERY NO: 970 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
970, 'Y', 'Alleghany', '', '', ''
);

/* INSERT QUERY NO: 971 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
971, 'GENZ', 'Genzyme', 'genzyme.com', '', ''
);

/* INSERT QUERY NO: 972 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
972, 'MTZ', 'MasTec', 'mastec.com', '', ''
);

/* INSERT QUERY NO: 973 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
973, 'CF', 'Genuity', 'genuity.com', '', ''
);

/* INSERT QUERY NO: 974 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
974, 'NFB', 'North Fork Bancorp.', 'northforkbank.com', '', ''
);

/* INSERT QUERY NO: 975 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
975, 'GTN', 'Grey Global', 'greyglobalgroup.com', '', ''
);

/* INSERT QUERY NO: 976 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
976, 'IVX', 'Ivax', 'ivax.com', '', ''
);

/* INSERT QUERY NO: 977 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
977, 'AMC', 'AMC Entertainment', 'amctheatres.com', '', ''
);

/* INSERT QUERY NO: 978 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
978, 'ONNN', 'On Semiconductor', 'onsemi.com', '', ''
);

/* INSERT QUERY NO: 979 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
979, 'FMAX', 'Software Spectrum', 'softwarespectrum.com', '', ''
);

/* INSERT QUERY NO: 980 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
980, 'VIAS', 'Viasystems Group', 'viasystems.com', '', ''
);

/* INSERT QUERY NO: 981 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
981, 'GGC', 'Georgia Gulf', 'ggc.com', '', ''
);

/* INSERT QUERY NO: 982 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
982, 'FRX', 'Forest Laboratories', 'frx.com', '', ''
);

/* INSERT QUERY NO: 983 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
983, 'PER', 'Perot Systems', 'perotsystems.com', '', ''
);

/* INSERT QUERY NO: 984 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
984, 'TRMP', 'Trump Hotels & Casino Resorts', 'trump.com', '', ''
);

/* INSERT QUERY NO: 985 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
985, 'FCH', 'FelCor Lodging', 'felcor.com', '', ''
);

/* INSERT QUERY NO: 986 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
986, 'DPL', 'DPL', 'dplinc.com', '', ''
);

/* INSERT QUERY NO: 987 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
987, 'IGT', 'International Game Technology', 'igt.com', '', ''
);

/* INSERT QUERY NO: 988 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
988, 'BTH', 'Blyth', 'blythinc.com', '', ''
);

/* INSERT QUERY NO: 989 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
989, 'TCF', 'TCF Financial Corp.', 'tcfbank.com', '', ''
);

/* INSERT QUERY NO: 990 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
990, 'TPX', 'Sealy', 'sealy.com', '', ''
);

/* INSERT QUERY NO: 991 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
991, 'SRCT', 'Standard Register', 'stdreg.com', '', ''
);

/* INSERT QUERY NO: 992 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
992, 'EMES', 'eMerge Interactive', 'emergeinteractive.com', '', ''
);

/* INSERT QUERY NO: 993 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
993, 'HDLM', 'Handleman', 'handleman.com', '', ''
);

/* INSERT QUERY NO: 994 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
994, 'GDYS', 'Goody\'s Family Clothing', 'goodysonline.com', '', ''
);

/* INSERT QUERY NO: 995 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
995, 'ALEX', 'Alexander & Baldwin', 'alexanderbaldwin.com', '', ''
);

/* INSERT QUERY NO: 996 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
996, 'DZTK', 'Daisytek International', 'daisytek.com', '', ''
);

/* INSERT QUERY NO: 997 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
997, 'TBL', 'Timberland', 'timberland.com', '', ''
);

/* INSERT QUERY NO: 998 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
998, 'AMS', 'American Management Systems', 'ams.com', '', ''
);

/* INSERT QUERY NO: 999 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
999, 'BCR', 'C.R. Bard', 'crbard.com', '', ''
);

/* INSERT QUERY NO: 1000 */
INSERT INTO tickers(id, ticker, company_name, domain, logo, updated_at)
VALUES
(
1000, 'CAJ', 'Canon Inc.', ' usa.canon.com', '', ''
);
