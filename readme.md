#Install

Run composer install:

    composer install

Setup your environment:
   - rename .env.example file to .env
  

Bootstrap the application:

    php artisan key:generate
    php artisan migrate
    import table tickers (please look for the file in the path /database/tickers.sql)


Run the application

    Registrate and login
    Run the command php artisan store:logos
    Go to /home view page
    
    
