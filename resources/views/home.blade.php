@extends('layouts.app')

@section('content')

<div class="container">
    <?php
    //Columns must be a factor of 12 (1,2,3,4,6,12)
    $numOfCols = 6;
    $rowCount = 0;
    $bootstrapColWidth = 12 / $numOfCols;
    foreach ($tickers as $ticker){
    if($rowCount % $numOfCols == 0) { ?> <div class="row"> <?php }
        $rowCount++; ?>
        <div class="col-md-<?php echo $bootstrapColWidth; ?>">
            <div class="thumbnail">
                <img src="{{$ticker->pictureUrl()}}" alt="" style="width:100%">
                <div class="container">
                <h4><b>{{$ticker->ticker}}</b></h4>
                <h8>{{$ticker->company_name}}</h8>
            </div>
            </div>
        </div>
        <?php
        if($rowCount % $numOfCols == 0) { ?> </div> <?php } } ?>
        {{ $tickers->links() }}
</div>
@endsection

